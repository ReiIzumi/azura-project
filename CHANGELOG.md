# AzuraModuleFileNet v1.0.2 (2018-01-26)
- Fixed an error generating the list of OptionList from FileNet

# AzuraClass v1.0.1 (2018-11-11)
**Libraries:**
- AzuraClassService 1.0.1
    - Import Document Class 1.0.1
- AzuraModuleCommons 1.0.1
- AzuraModuleFileNet 1.0.1

**Azura:**
- Updated the order of importing DocumentClass, now the Property/Option List is imported first and then the DocumentClass to add its properties

**Module Commons:**
- Added the metadata in the toString functionality of AzProperty

**Module FileNet:**
- Now fields that come from parent classes are filtered correctly, before only the fields of the direct father were filtered
- Now the properties are added to the DocumentClass and those that no longer exist are deleted, before the properties were neither added or deleted
- Fixed an error in which the Symbolic Name of the properties was lost when returning the result after creating it or updating it

# AzuraClass v1.0.0 (2018-10-20)
Current services:
- List all Document Class
- List all Property
- List all Option List
- Export Document Class (all or according to the list)
- Export Property (all or according to the list)
- Export Option List (all or according to the list)
- Import Document Class
- Import Property
- Import Option List