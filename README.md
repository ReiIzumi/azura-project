# Azura

## Descripción
Azura es una aplicación Java que permite la importación y exportación de sistemas Content Manager (CM). Cada proceso se ejecuta independiente, pudiendo extraer información de un sistema para importarla en otro entorno.

La aplicación genera un sistema de ficheros propio para exportar y estos son utilizados para importar (pudiéndolos crear desde una exportación previa o generándolos a mano). La conexión con el sistema CM se crea a partir de módulos de Azura que están escritos específicamente para un CM concreto, si se debe conectar a un CM del cual no existe el módulo, este deberá ser creado.

Azura se divide en dos secciones:
- Configuración (Class)
- Documentos (Document)

La configuración se centra en los Document Class, Property y Option List.<br />
Los documentos tienen los metadatos y contenidos así como sus carpetas.

## Documentación
Toda la documentación se encuentra en la [Wiki](/../wikis/home).

## Instalación
Descargar la última versión desde [aquí](/../tags) y descomprimir en la ubicación final. **Únicamente disponible el Class, Document sigue en desarrollo**.<br />
Actualizar la configuración de los ficheros en la carpeta config, por defecto la configuración está preparada para utilizar el módulo de FileNet (el único existente), así que principalmente requiere actualizar los ficheros **AzuraClass.properties** y **ModuleFileNet.properties** que se encuentran en la carpeta config.

### Dependencias
Azura es una aplicación Java Batch, creada sobre Java 6, está diseñada para funcionar sobre Oracle Java, OpenJDK o IBM Java, aunque en el caso de utilizarla para FileNet, se recomienda el uso de IBM Java.<br />
Acorde a la configuración por defecto, todos los ficheros generados por Azura están dentro de sus carpetas, así que requiere permisos de escritura sobre todos ellos.<br />
También requiere que los puertos hacia el servidor de CM estén abiertos y un usuario que tenga permisos para conectar a este.

### Configuración rápida
Por defecto, la configuración actual ya es suficiente para utilizar todos los servicios con FileNet, únicamente se requiere los datos del servidor de FileNet (URL, User, Password y ObjectStore) que están en el fichero ModuleFilenet.properties.<br />
En caso de querer utilizar el Import modificando los metadatos de creación, el usuario tiene que tener el permiso especial de "Modify certain system properties".

**Para FileNet, se debe añadir las librerías de Jace acorde a la versión utilizada**

### Configuración completa
En la [Wiki](/../wikis/configurate) se encuentra la explicación de todos los ficheros de configuración.

### Uso
Cada servicio tiene un fichero .sh y .bat para ejecutarlo más fácilmente.

Estos son los servicios disponibles:
- List: Genera una lista de todos los ID, ID Name y Name de todos los elementos existentes. La lista se encuentra en la carpeta list.
- Export: Exporta todos los elementos al formato JSON de Azura, se puede enviar un segundo argumento con la lista de ID (separados por coma) para exportar únicamente estos. Se guardan en la carpeta export/class. Se genera un report con la lista de todos los ficheros exportados.
- Import: Todos los JSON en la carpeta import/class/source se importan hacia el CM de destino, estos son movidos a las carpetas de processed o error. Se generan dos reports, uno acorde al resultado de cada fichero y otro para cada acción tomada.

Cada servicio se divide en Document Class, Property o Option List, aunque en el caso de Export/Import, Document Class también es capaz de exportar/importar los Property que utilice y Property es capaz de exportar/importar los Option list que utilice.

Recordar que para importar un Document Class, debe existir su Document Class padre, de no existir no será importado. Si se importan varios Document Class, Azura se encargará de importar primero los padres y después los hijos.

## Changelog
Lista de [cambios](CHANGELOG.md).

## Licencia
El proyecto está bajo la licencia [MIT](LICENSE.md).

### Licencias de terceros
[Aquí](/../wikis/licenses) se encuentra una lista de las librerías y licencias de terceros que utiliza el proyecto.