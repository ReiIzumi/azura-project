package cat.moon.azura.constant;

public class ServiceConstant {
	public static final String DOCUMENT_CLASS	= "DocumentClass";
	public static final String PROPERTY			= "Property";
	public static final String OPTION_LIST		= "OptionList";
}
