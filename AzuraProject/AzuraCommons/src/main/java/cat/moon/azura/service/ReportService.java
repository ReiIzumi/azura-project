package cat.moon.azura.service;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.util.Date;

import org.apache.log4j.Logger;

import cat.moon.azura.constant.DelimiterConstant;
import cat.moon.azura.constant.ErrorCodeCommons;
import cat.moon.azura.constant.PropConstant;
import cat.moon.azura.exception.AzException;
import cat.moon.azura.util.FileUtil;
import cat.moon.azura.util.PropUtil;

public class ReportService {
	private static Logger logger = Logger.getLogger(ReportService.class);
	
	protected static final String FILENAME			= "Report_";
	protected static final String CSV_EXTENSION		= "csv";
	protected static final String TXT_EXTENSION		= "txt";
	
	protected String delimiter;
	protected String folderPath;
	protected String datePattern;
	protected boolean generateTxt;
	protected Date date;
	protected String serviceName;
	
	protected BufferedWriter bw = null;
	protected FileWriter fw = null;
	protected BufferedWriter bwTxt = null;
	protected FileWriter fwTxt = null;
	
	protected ReportService(PropUtil prop, String reportFolder, String serviceName) throws AzException {
		logger.info("> Initializing Report Service");
		date = new Date();
		
		this.serviceName = serviceName;
		
		delimiter = prop.get(PropConstant.REPORT_CSV_DELIMITER, DelimiterConstant.DELIMITER);
		folderPath = FileUtil.getFolderPath(prop.get(reportFolder));
		logger.info(">> Report folder: "+folderPath);
		datePattern = prop.get(PropConstant.REPORT_FILE_DATEPATTERN, null);
		
		if("Y".equals(prop.get(PropConstant.REPORT_GENERATE_TXT))) generateTxt = true;
		else generateTxt = false;
	}
	
	protected void initializeFiles() throws AzException {
		File file = FileUtil.initializeFile(FILENAME, serviceName, CSV_EXTENSION, datePattern, date, folderPath);
		File fileTxt = null;;
		if(generateTxt) {
			fileTxt = FileUtil.initializeFile(FILENAME, serviceName, TXT_EXTENSION, datePattern, date, folderPath);
		}
		
		logger.info(">> Report file: "+file.getName());
		if(generateTxt) logger.info(">> Report file (TXT): "+fileTxt.getName());
		
		/* Prepare file */
		try {
			fw = new FileWriter(file);
			bw = new BufferedWriter(fw);
			if(generateTxt) {
				fwTxt = new FileWriter(fileTxt);
				bwTxt = new BufferedWriter(fwTxt);
			}
		} catch(Exception e) {
			throw new AzException(ErrorCodeCommons.AZCOM_08, e);
		}
	}
	
	public void close() {
		if(bw != null) try {bw.close();} catch (Exception e) {}
		if(fw != null) try {fw.close();} catch (Exception e) {}
		if(bwTxt != null) try {bwTxt.close();} catch (Exception e) {}
		if(fwTxt != null) try {fwTxt.close();} catch (Exception e) {}
	}
}
