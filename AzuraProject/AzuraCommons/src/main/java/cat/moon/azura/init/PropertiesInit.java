package cat.moon.azura.init;

import java.io.FileReader;
import java.util.Properties;

import cat.moon.azura.constant.ErrorCodeCommons;
import cat.moon.azura.exception.AzException;
import cat.moon.azura.util.PropUtil;

public class PropertiesInit {
	public static PropUtil init(String propertiesPath) throws AzException {
		FileReader fr = null;
		PropUtil propUtil = null;
		try {
			fr = new FileReader(propertiesPath);
			Properties prop = new Properties();
			prop.load(fr);
			propUtil = new PropUtil(prop);
		} catch(Exception e) {
			throw new AzException(ErrorCodeCommons.NO_PROPERTIES);
		} finally {
			if(fr != null) try {fr.close();} catch (Exception e) {}
		}
		return propUtil;
	}
}
