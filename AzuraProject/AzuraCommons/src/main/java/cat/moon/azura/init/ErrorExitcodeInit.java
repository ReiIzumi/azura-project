package cat.moon.azura.init;

import java.io.File;
import java.io.FileReader;
import java.util.Properties;

import cat.moon.azura.constant.ErrorCodeCommons;
import cat.moon.azura.constant.PropConstant;
import cat.moon.azura.exception.AzException;
import cat.moon.azura.util.PropUtil;

public class ErrorExitcodeInit {
	public static PropUtil init(PropUtil propUtil) throws AzException {
		PropUtil errCodeProp = null;
		
		String errorExitcodePath = propUtil.get(PropConstant.ERROR_EXITCODE);
		if(errorExitcodePath != null && errorExitcodePath.isEmpty()) {
			File f = new File(errorExitcodePath);
			if(!f.isFile()) {
				throw new AzException(ErrorCodeCommons.ERRCODE_NOT_EXIST);
			}
			FileReader fr = null;
			try {
				fr = new FileReader(f);
				Properties prop = new Properties();
				prop.load(fr);
				errCodeProp = new PropUtil(prop);
			} catch(Exception e) {
				throw new AzException(ErrorCodeCommons.ERRORCODE_UNABLE);
			} finally {
				if(fr != null) try {fr.close();} catch (Exception e) {}
			}
		}
		return errCodeProp;
	}
}
