package cat.moon.azura.service;

import java.io.FileReader;
import java.util.Properties;

import org.apache.log4j.Logger;

import cat.moon.azura.constant.ErrorCodeCommons;
import cat.moon.azura.constant.PropConstant;
import cat.moon.azura.exception.AzException;
import cat.moon.azura.module.IClassModule;
import cat.moon.azura.module.IDocumentModule;
import cat.moon.azura.util.PropUtil;

public class ModuleService {
	private static Logger logger = Logger.getLogger(ModuleService.class);
	
	public static IClassModule getClassModule(PropUtil prop) throws AzException {
		logger.info("> Initializing Class Module");
		
		String moduleClass = getModule(prop);
		PropUtil moduleProp = getProperty(prop);
		
		IClassModule module = null;
		try {
			module = (IClassModule) Class.forName(moduleClass).newInstance();
		} catch(Exception e) {
			throw new AzException(ErrorCodeCommons.AZCOM_03.extraMessage(moduleClass), e);
		}
		try {
			module.init(prop, moduleProp);
		} catch(AzException e) {
			throw e;
		} catch(Exception e) {
			throw new AzException(ErrorCodeCommons.AZCOM_04, e);
		}
		return module;
	}
	
	public static IDocumentModule getDocumentModule(PropUtil prop) throws AzException {
		logger.info("> Initializing Documet Module");
		
		String moduleClass = getModule(prop);
		PropUtil moduleProp = getProperty(prop);
		
		IDocumentModule module = null;
		try {
			module = (IDocumentModule) Class.forName(moduleClass).newInstance();
		} catch(Exception e) {
			throw new AzException(ErrorCodeCommons.AZCOM_03.extraMessage(moduleClass), e);
		}
		try {
			module.init(prop, moduleProp);
		} catch(AzException e) {
			throw e;
		} catch(Exception e) {
			throw new AzException(ErrorCodeCommons.AZCOM_04, e);
		}
		return module;
	}
	
	private static String getModule(PropUtil prop) throws AzException {
		String moduleClass = prop.get(PropConstant.MODULE_CLASS);
		if(moduleClass == null || moduleClass.isEmpty()) 
			throw new AzException(ErrorCodeCommons.AZCOM_01);
		return moduleClass;
		
	}
	
	private static PropUtil getProperty(PropUtil prop) throws AzException {
		PropUtil moduleProp = null;
		String moduleClassProp = prop.get(PropConstant.MODULE_PROPERTIES);
		if(moduleClassProp != null && !moduleClassProp.isEmpty()) {
			FileReader fr = null;
			try {
				fr = new FileReader(moduleClassProp);
				Properties p = new Properties();
				p.load(fr);
				moduleProp = new PropUtil(p);
			} catch(Exception e) {
				throw new AzException(ErrorCodeCommons.AZCOM_02.extraMessage(moduleClassProp));
			} finally {
				if(fr != null) try {fr.close();} catch (Exception e) {}
			}
		}
		return moduleProp;
	}
}
