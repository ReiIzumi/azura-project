package cat.moon.azura.util;

import java.io.File;
import java.text.SimpleDateFormat;
import java.util.Date;

import org.apache.log4j.Logger;

import cat.moon.azura.constant.ErrorCodeCommons;
import cat.moon.azura.exception.AzException;

public class FileUtil {
	private static Logger logger = Logger.getLogger(FileUtil.class);
	
	public static File getFolder(String path) throws AzException {
		if(path == null || path.isEmpty()) throw new AzException(ErrorCodeCommons.AZCOM_05);
		File f = new File(path);
		if(!f.isDirectory()) throw new AzException(ErrorCodeCommons.AZCOM_10.extraMessage(f.getAbsolutePath()));
		return f;
	}
	
	public static String getFolderPath(String path) throws AzException {
		if(path == null || path.isEmpty()) throw new AzException(ErrorCodeCommons.AZCOM_05);
		File f = new File(path);
		try {
			if(!f.isDirectory()) f.mkdirs();
		} catch(Exception e) {
			throw new AzException(ErrorCodeCommons.AZCOM_06.extraMessage(f.getAbsolutePath()), e);
		}
		return f.getAbsolutePath()+"\\";
	}
	
	public static String getFilename(String name, String extension, String datePattern) {
		if(datePattern == null) {
			return name+"."+extension;
		} else {
			SimpleDateFormat sdf = new SimpleDateFormat(datePattern);
			return name+"_"+sdf.format(new Date())+"."+extension;
		}
	}
	
	public static String getFilename(String name, String extension, String datePattern, Date date) {
		if(datePattern == null) {
			return name+"."+extension;
		} else {
			SimpleDateFormat sdf = new SimpleDateFormat(datePattern);
			return name+"_"+sdf.format(date)+"."+extension;
		}
	}
	
	public static File getFile(String path, String filename) {
		return new File(path+filename);
	}

	public static void deleteOldFile(File file) throws AzException {
		if(file.isFile()) {
			if(logger.isDebugEnabled()) logger.debug("Deleting old file: "+file.getAbsolutePath());
			try {
				file.delete();
			} catch(Exception e) {
				throw new AzException(ErrorCodeCommons.AZCOM_07.extraMessage(file.getAbsolutePath()), e);
			}
		}
	}
	
	public static File initializeFile(String prefix, String serviceName, String extension, 
			String datePattern, Date date, String folderPath) throws AzException {
		
		String filename = FileUtil.getFilename(prefix+serviceName, extension, datePattern, date);
		File file = FileUtil.getFile(folderPath, filename);
		FileUtil.deleteOldFile(file);
		return file;
	}
}
