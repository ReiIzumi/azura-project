package cat.moon.azura.constant;

import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

public class ActionConstant {
	public static final int CREATE						= 0;
	public static final int UPDATE_ALWAYS				= 1;
	public static final int UPDATE_MODIFIED				= 2;
	public static final int DISCARD						= 3;
	
	public static final String CREATE_AS_STR			= "CREATE";
	public static final String UPDATE_ALWAYS_AS_STR		= "UPDATE_ALWAYS";
	public static final String UPDATE_MODIFIED_AS_STR	= "UPDATE_MODIFIED";
	public static final String DISCARD_AS_STR			= "DISCARD";
	
	public static final Map<Integer, String> TEXT;
	public static final Map<String, Integer> INT;
	
	static {
		Map<Integer, String> t = new HashMap<Integer, String>();
		t.put(CREATE, CREATE_AS_STR);
		t.put(UPDATE_ALWAYS, UPDATE_ALWAYS_AS_STR);
		t.put(UPDATE_MODIFIED, UPDATE_MODIFIED_AS_STR);
		t.put(DISCARD, DISCARD_AS_STR);
		TEXT = Collections.unmodifiableMap(t);
		
		Map<String, Integer> i = new HashMap<String, Integer>();
		i.put(CREATE_AS_STR, CREATE);
		i.put(UPDATE_ALWAYS_AS_STR, UPDATE_ALWAYS);
		i.put(UPDATE_MODIFIED_AS_STR, UPDATE_MODIFIED);
		i.put(DISCARD_AS_STR, DISCARD);
		INT = Collections.unmodifiableMap(i);
	}
}
