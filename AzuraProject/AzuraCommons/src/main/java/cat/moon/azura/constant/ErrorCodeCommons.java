package cat.moon.azura.constant;

import cat.moon.azura.bean.generic.ErrorMessage;

public class ErrorCodeCommons {
	/* Error code for invalid arguments, properties and error exit code properties */
	public static final int START_EXITCODE				= 999;
	public static final ErrorMessage NO_PROPERTIES		= new ErrorMessage(START_EXITCODE, "NO_PROPERTIES", "Property file is not found in first argument or does not exist");
	public static final ErrorMessage ERRCODE_NOT_EXIST	= new ErrorMessage(START_EXITCODE, "ERRCODE_NOT_EXIST", "Error code property not exist");
	public static final ErrorMessage ERRORCODE_UNABLE	= new ErrorMessage(START_EXITCODE, "ERRORCODE_UNABLE", "Unable to start the error code system");
	
	/* Default error - ONLY to uncontrolled errors */
	public static final int DEFAULT_ERROR_EXITCODE		= 998;
	public static final ErrorMessage DEFAULT_ERROR		= new ErrorMessage(DEFAULT_ERROR_EXITCODE, "DEFAULT_ERROR", "An uncontrolled error by the system has ocurred");
	
	public static final ErrorMessage AZCOM_01			= new ErrorMessage("AZCOM_01", "Module not configured");
	public static final ErrorMessage AZCOM_02			= new ErrorMessage("AZCOM_02", "Properties Module configured but not exist/readable");
	public static final ErrorMessage AZCOM_03			= new ErrorMessage("AZCOM_03", "Error initializing the module, it may not exist");
	public static final ErrorMessage AZCOM_04			= new ErrorMessage("AZCOM_04", "Something did not work when starting the module");
	public static final ErrorMessage AZCOM_05			= new ErrorMessage("AZCOM_05", "Folder not configured");
	public static final ErrorMessage AZCOM_06			= new ErrorMessage("AZCOM_06", "Folder cannot be created");
	public static final ErrorMessage AZCOM_07			= new ErrorMessage("AZCOM_07", "Cannot delete the old file");
	public static final ErrorMessage AZCOM_08			= new ErrorMessage("AZCOM_08", "Error initializing the file buffer");
	public static final ErrorMessage AZCOM_09			= new ErrorMessage("AZCOM_09", "Error writing the file buffer");
	public static final ErrorMessage AZCOM_10			= new ErrorMessage("AZCOM_10", "Folder does not exist");
	
	
}
