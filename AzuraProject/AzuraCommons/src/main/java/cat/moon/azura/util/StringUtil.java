package cat.moon.azura.util;

import org.apache.commons.lang.StringUtils;

public class StringUtil {
	public static String addSpaces(String text, int length) {
		if(text == null) text = "";
		return String.format("%-"+length+"s", text);
	}
	
	public static String repeatChar(String c, int length) {
		return StringUtils.repeat(c, length);
	}
}
