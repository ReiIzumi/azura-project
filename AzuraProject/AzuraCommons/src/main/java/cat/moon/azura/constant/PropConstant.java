package cat.moon.azura.constant;

public class PropConstant {
	public static final String ERROR_EXITCODE						= "error.exitcode";
	public static final String LOG_PATH								= "log.path";
	
	public static final String MODULE_CLASS							= "module.class";
	public static final String MODULE_PROPERTIES					= "module.properties";
	
	public static final String CLASS_LIST_FOLDER					= "class.list.folder";
	public static final String CLASS_EXPORT_FOLDER					= "class.export.folder";
	public static final String DOCUMENT_LIST_FOLDER					= "document.list.folder";
	public static final String DOCUMENT_EXPORT_FOLDER				= "document.export.folder";
	
	public static final String CLASS_IMPORT_SOURCE					= "class.import.source";
	public static final String CLASS_IMPORT_ERROR					= "class.import.error";
	public static final String CLASS_IMPORT_PROCESSED				= "class.import.processed";
	public static final String CLASS_DOCUMENT_SOURCE				= "class.document.source";
	public static final String CLASS_DOCUMENT_ERROR					= "class.document.error";
	public static final String CLASS_DOCUMENT_PROCESSED				= "class.document.processed";
	
	public static final String LIST_CSV_DELIMITER					= "list.csv.delimiter";
	public static final String LIST_FILE_DATEPATTERN				= "list.file.datePattern";	
	public static final String LIST_GENERATE_TXT					= "list.generate.txt";
	public static final String LIST_GENERATE_TXT_ID_LENGTH			= "list.generate.txt.id.length";
	public static final String LIST_GENERATE_TXT_IDNAME_LENGTH		= "list.generate.txt.idName.length";
	public static final String LIST_GENERATE_TXT_NAME_LENGTH		= "list.generate.txt.name.length";
	
	public static final String IMPORT_DOCUMENTCLASS_ALWAYS_UPDATE	= "import.documentClass.alwaysUpdate";
	public static final String IMPORT_PROPERTY_ALWAYS_UPDATE		= "import.property.alwaysUpdate";
	public static final String IMPORT_OPTIONLIST_ALWAYS_UPDATE		= "import.optionList.alwaysUpdate";
	public static final String IMPORT_DOCUMENTCLASS_UPDATE_NEWER	= "import.documentClass.updateIfNewer";
	public static final String IMPORT_PROPERTY_UPDATE_NEWER			= "import.property.updateIfNewer";
	public static final String IMPORT_OPTIONLIST_UPDATE_NEWER		= "import.optionList.updateIfNewer";
	
	public static final String IMPORT_CLASS_METADATA				= "import.class.metadata";
	public static final String IMPORT_DOCUMENT_METADATA				= "import.document.metadata";
	
	public static final String REPORT_CSV_DELIMITER					= "report.csv.delimiter";
	public static final String REPORT_FILE_DATEPATTERN				= "report.file.datePattern";	
	
	public static final String REPORT_GENERATE_TXT					= "report.generate.txt";
	public static final String REPORT_GENERATE_TXT_ID_LENGTH		= "report.generate.txt.id.length";
	public static final String REPORT_GENERATE_TXT_IDNAME_LENGTH	= "report.generate.txt.idName.length";
	public static final String REPORT_GENERATE_TXT_NAME_LENGTH		= "report.generate.txt.name.length";
	/* Only import */
	public static final String REPORT_GENERATE_TXT_TARGET_LENGTH	= "report.generate.txt.target.length";
	public static final String REPORT_GENERATE_TXT_TIME_LENGTH		= "report.generate.txt.time.length";
	public static final String REPORT_GENERATE_TXT_FILE_LENGTH		= "report.generate.txt.file.length";
	public static final String REPORT_GENERATE_TXT_TYPE_LENGTH		= "report.generate.txt.type.length";
	public static final String REPORT_GENERATE_TXT_ACTION_LENGTH	= "report.generate.txt.action.length";
	public static final String REPORT_GENERATE_TXT_ERROR_CODE_LENGTH= "report.generate.txt.error.code.length";
	public static final String REPORT_GENERATE_TXT_ERROR_CODEMESSAGE_LENGTH="report.generate.txt.error.codemessage.length";
	public static final String REPORT_GENERATE_TXT_ERROR_MESSAGE_LENGTH="report.generate.txt.error.message.length";
	
	public static final String REPORT_CLASS_EXPORT_FOLDER			= "report.class.export.folder";
	public static final String REPORT_CLASS_IMPORT_FOLDER			= "report.class.import.folder";
	public static final String REPORT_DOCUMENT_EXPORT_FOLDER		= "report.document.export.folder";
	public static final String REPORT_DOCUMENT_IMPORT_FOLDER		= "report.document.import.folder";

}
