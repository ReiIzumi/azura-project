package cat.moon.azura.init;

import java.io.File;

import org.apache.log4j.BasicConfigurator;
import org.apache.log4j.Logger;
import org.apache.log4j.PropertyConfigurator;

import cat.moon.azura.constant.PropConstant;
import cat.moon.azura.util.PropUtil;

public class Log4jInit {
	private static Logger logger = Logger.getLogger(Log4jInit.class);
	
	public static void init(PropUtil propUtil) {
		String log = propUtil.get(PropConstant.LOG_PATH);
		if(log == null || log.isEmpty()) BasicConfigurator.configure();
		else {
			File f = new File(log);
			if(f.isFile()) {
				try {
					PropertyConfigurator.configure(log);
				} catch(Exception e) {
					BasicConfigurator.configure();
					logger.warn("Error initializing log4j, the basic configuration will be used");
				}
			} else {
				BasicConfigurator.configure();
				logger.warn("log4j.properties not found: "+f.getAbsolutePath()
						+ ", the basic configuration will be used");
			}
			
		}
	}
}
