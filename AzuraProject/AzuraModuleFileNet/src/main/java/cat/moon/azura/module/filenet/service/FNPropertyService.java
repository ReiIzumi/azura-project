package cat.moon.azura.module.filenet.service;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import org.apache.log4j.Logger;

import com.filenet.api.admin.LocalizedString;
import com.filenet.api.admin.PropertyTemplate;
import com.filenet.api.admin.PropertyTemplateBinary;
import com.filenet.api.admin.PropertyTemplateBoolean;
import com.filenet.api.admin.PropertyTemplateDateTime;
import com.filenet.api.admin.PropertyTemplateFloat64;
import com.filenet.api.admin.PropertyTemplateId;
import com.filenet.api.admin.PropertyTemplateInteger32;
import com.filenet.api.admin.PropertyTemplateString;
import com.filenet.api.collection.IndependentObjectSet;
import com.filenet.api.constants.Cardinality;
import com.filenet.api.constants.ClassNames;
import com.filenet.api.constants.PropertyNames;
import com.filenet.api.constants.TypeID;
import com.filenet.api.core.Factory;
import com.filenet.api.core.ObjectStore;
import com.filenet.api.core.UpdatingBatch;
import com.filenet.api.query.SearchSQL;
import com.filenet.api.query.SearchScope;
import com.filenet.api.util.Id;

import cat.moon.azura.bean.AzProperty;
import cat.moon.azura.bean.AzPropertyBinary;
import cat.moon.azura.bean.AzPropertyBoolean;
import cat.moon.azura.bean.AzPropertyDate;
import cat.moon.azura.bean.AzPropertyDouble;
import cat.moon.azura.bean.AzPropertyId;
import cat.moon.azura.bean.AzPropertyInteger;
import cat.moon.azura.bean.AzPropertyString;
import cat.moon.azura.bean.generic.AzList;
import cat.moon.azura.bean.response.ImportResponse;
import cat.moon.azura.constant.CardinalityConstant;
import cat.moon.azura.constant.TypeConstant;
import cat.moon.azura.exception.AzException;
import cat.moon.azura.module.filenet.constant.ErrorCodeModule;
import cat.moon.azura.module.filenet.util.IdUtil;
import cat.moon.azura.module.filenet.util.SQLUtils;

public class FNPropertyService {
	private static Logger logger = Logger.getLogger(FNPropertyService.class);
	
	public static List<AzList> list(ObjectStore os, int pageSize) {
		List<AzList> list = new ArrayList<AzList>();
		String sql = SQLUtils.createQuery(ClassNames.PROPERTY_TEMPLATE, null, 
				PropertyNames.SYMBOLIC_NAME+" ASC");
		
		SearchScope search = new SearchScope(os);
		SearchSQL searchSQL = new SearchSQL(sql);
		IndependentObjectSet set = search.fetchObjects(searchSQL, pageSize, null, true);
		if(!set.isEmpty()) {
			@SuppressWarnings("unchecked")
			Iterator<PropertyTemplate> it = set.iterator();
			AzList l;
			PropertyTemplate impl;
			while(it.hasNext()) {
				impl = it.next();
				l = new AzList();
				l.setId(IdUtil.toString(impl.get_Id()));
				l.setIdName(impl.get_SymbolicName());
				l.setName(impl.get_Name());
				list.add(l);
			}
		} else logger.info("Nothing found");
		return list;
	}
	
	public static Map<String, Date> listId(ObjectStore os, int pageSize) {
		Map<String, Date> list = new HashMap<String, Date>();
		String sql = SQLUtils.createQuery(ClassNames.PROPERTY_TEMPLATE, null, null);
		
		SearchScope search = new SearchScope(os);
		SearchSQL searchSQL = new SearchSQL(sql);
		IndependentObjectSet set = search.fetchObjects(searchSQL, pageSize, null, true);
		if(!set.isEmpty()) {
			@SuppressWarnings("unchecked")
			Iterator<PropertyTemplate> it = set.iterator();
			PropertyTemplate impl;
			while(it.hasNext()) {
				impl = it.next();
				list.put(impl.get_SymbolicName(), impl.get_DateLastModified());
			}
		}
		return list;
	}
	
	public static List<AzProperty> get(ObjectStore os, int pageSize, List<String> ids) {
		List<AzProperty> list = new ArrayList<AzProperty>();
		
		String sql = SQLUtils.createQuery(ClassNames.PROPERTY_TEMPLATE, ids);
		
		SearchScope search = new SearchScope(os);
		SearchSQL searchSQL = new SearchSQL(sql);
		IndependentObjectSet set = search.fetchObjects(searchSQL, pageSize, null, true);
		if(!set.isEmpty()) {
			@SuppressWarnings("unchecked")
			Iterator<PropertyTemplate> it = set.iterator();
			while(it.hasNext()) {
				list.add(getProperty(it.next()));
			}
		} else logger.info("Nothing found");;
		return list;
	}
	
	@SuppressWarnings("unchecked")
	public static Iterator<PropertyTemplate> getPropertyDefinitionList(ObjectStore os, int pageSize, List<String> symbolicNames) {
		String sql = SQLUtils.createQuerySymbolicName(ClassNames.PROPERTY_TEMPLATE, symbolicNames);
		
		SearchScope search = new SearchScope(os);
		SearchSQL searchSQL = new SearchSQL(sql);
		IndependentObjectSet set = search.fetchObjects(searchSQL, pageSize, null, true);
		if(!set.isEmpty()) {
			return set.iterator();
		} else logger.info("No properties found");
		return null;
	}
	
	public static AzProperty getProperty(PropertyTemplate impl) {
		if(logger.isDebugEnabled()) logger.debug("Id: "+impl.get_Id().toString()+" / Name: "+impl.get_Name());
		AzProperty value = new AzProperty();
		value.setId(IdUtil.toString(impl.get_Id()));
		value.setIdName(impl.get_SymbolicName());
		value.setName(impl.get_Name());
		value.setDescription(impl.get_DescriptiveText());
		value.setCreator(impl.get_Creator());
		value.setCreated(impl.get_DateCreated());
		value.setModifier(impl.get_LastModifier());
		value.setModified(impl.get_DateLastModified());
		
		if(TypeID.STRING.equals(impl.get_DataType())) value.setType(TypeConstant.STRING);
		else if(TypeID.LONG.equals(impl.get_DataType())) value.setType(TypeConstant.INTEGER);
		else if(TypeID.DOUBLE.equals(impl.get_DataType())) value.setType(TypeConstant.DOUBLE);
		else if(TypeID.DATE.equals(impl.get_DataType())) value.setType(TypeConstant.DATE);
		else if(TypeID.BOOLEAN.equals(impl.get_DataType())) value.setType(TypeConstant.BOOLEAN);
		else if(TypeID.GUID.equals(impl.get_DataType())) value.setType(TypeConstant.ID);
		else if(TypeID.OBJECT.equals(impl.get_DataType())) value.setType(TypeConstant.OBJECT);
		else if(TypeID.BINARY.equals(impl.get_DataType())) value.setType(TypeConstant.BINARY);
		
		if(Cardinality.SINGLE == impl.get_Cardinality()) {
			value.setMultivalue(false);
		} else {
			value.setMultivalue(true);
			if(impl.get_RequiresUniqueElements()) value.setMultivalueCardinality(CardinalityConstant.MAP);
			else value.setMultivalueCardinality(CardinalityConstant.LIST);
		}
		
		if(impl.get_ChoiceList() != null) {
			value.setOptionList(FNChoiceListService.getOptionList(impl.get_ChoiceList()));
		}
		
		if(impl.get_IsValueRequired() == null || !impl.get_IsValueRequired()) value.setRequired(false);
		else value.setRequired(true);
		if(impl.get_IsHidden() == null || !impl.get_IsHidden()) value.setHidden(false);
		else value.setHidden(true);
		value.setCategory(impl.get_PropertyDisplayCategory());
		
		switch(value.getType()) {
			case TypeConstant.STRING: 
				PropertyTemplateString implStr = (PropertyTemplateString) impl;
				value.setSize(implStr.get_MaximumLengthString());
				AzPropertyString ps = new AzPropertyString();
				ps.setDefaultValue(implStr.get_PropertyDefaultString());
				value.setPropString(ps);
				break;
			case TypeConstant.INTEGER: 
				PropertyTemplateInteger32 implInt = (PropertyTemplateInteger32) impl;
				AzPropertyInteger pi = new AzPropertyInteger();
				pi.setDefaultValue(implInt.get_PropertyDefaultInteger32());
				pi.setMin(implInt.get_PropertyMinimumInteger32());
				pi.setMax(implInt.get_PropertyMaximumInteger32());
				value.setPropInteger(pi);
				break;
			case TypeConstant.DOUBLE: 
				PropertyTemplateFloat64 implFloat = (PropertyTemplateFloat64) impl;
				AzPropertyDouble pf = new AzPropertyDouble();
				pf.setDefaultValue(implFloat.get_PropertyDefaultFloat64());
				pf.setMin(implFloat.get_PropertyMinimumFloat64());
				pf.setMax(implFloat.get_PropertyMaximumFloat64());
				value.setPropDouble(pf);
				break;
			case TypeConstant.DATE: 
				PropertyTemplateDateTime implDate = (PropertyTemplateDateTime) impl;
				AzPropertyDate pda = new AzPropertyDate();
				pda.setDefaultValue(implDate.get_PropertyDefaultDateTime());
				pda.setMin(implDate.get_PropertyMinimumDateTime());
				pda.setMax(implDate.get_PropertyMaximumDateTime());
				value.setPropDate(pda);
				break;
			case TypeConstant.BOOLEAN: 
				PropertyTemplateBoolean implBoolean = (PropertyTemplateBoolean) impl;
				AzPropertyBoolean pb = new AzPropertyBoolean();
				pb.setDefaultValue(implBoolean.get_PropertyDefaultBoolean());
				value.setPropBoolean(pb);
				break;
			case TypeConstant.ID: 
				PropertyTemplateId implId = (PropertyTemplateId) impl;
				AzPropertyId pid = new AzPropertyId();
				if(implId.get_PropertyDefaultId() != null) 
					pid.setDefaultValue(implId.get_PropertyDefaultId().toString());
				value.setPropId(pid);
				break;
			case TypeConstant.OBJECT: 
				break;
			case TypeConstant.BINARY: 
				PropertyTemplateBinary implBinary = (PropertyTemplateBinary) impl;
				value.setSize(implBinary.get_MaximumLengthBinary());
				AzPropertyBinary pbi = new AzPropertyBinary();
				pbi.setDefaultValue(implBinary.get_PropertyDefaultBinary());
				value.setPropBinary(pbi);
				break;
		}
		return value;
	}
	
	public static ImportResponse add(AzProperty bean, boolean updateMetadata, 
			ObjectStore os, UpdatingBatch importBatch) throws AzException {
		
		Id id;
		if(bean.getId() == null || bean.getId().trim().isEmpty()) id = Id.createId();
		else id = new Id(bean.getId());
		
		PropertyTemplate property = null;
		
		switch(bean.getType()) {
			case TypeConstant.STRING:
				property = Factory.PropertyTemplateString.createInstance(os, id);
				break;
			case TypeConstant.INTEGER: 
				property = Factory.PropertyTemplateInteger32.createInstance(os, id);
				break;
			case TypeConstant.DOUBLE: 
				property = Factory.PropertyTemplateFloat64.createInstance(os, id);
				break;
			case TypeConstant.DATE: 
				property = Factory.PropertyTemplateDateTime.createInstance(os, id);
				break;
			case TypeConstant.BOOLEAN: 
				property = Factory.PropertyTemplateBoolean.createInstance(os, id);
				break;
			case TypeConstant.ID: 
				property = Factory.PropertyTemplateId.createInstance(os, id);
				break;
			case TypeConstant.OBJECT:
				property = Factory.PropertyTemplateObject.createInstance(os, id);
				break;
			case TypeConstant.BINARY:
				property = Factory.PropertyTemplateBinary.createInstance(os, id);
				break;
			default:
				throw new AzException(ErrorCodeModule.AZM_FN_IM_PR01.extraMessage(Integer.toString(bean.getType())));
		}
		
		ImportResponse response = save(IdUtil.toString(id), property, bean, 
				updateMetadata, true, os, importBatch);
		return response;
	}
	
	public static ImportResponse update(AzProperty bean, boolean updateMetadata, 
			ObjectStore os, UpdatingBatch importBatch) throws AzException {
		
		PropertyTemplate property = Factory.PropertyTemplate.fetchInstance(os, new Id(bean.getId()), null);
		
		property.get_DataType();
		if(bean.getType() == TypeConstant.STRING && !TypeID.STRING.equals(property.get_DataType())) 
			throw new AzException(ErrorCodeModule.AZM_FN_IM_PR02.extraMessage(property.get_DataType().toString()+" > "+TypeConstant.STRING_AS_STR));
		else if(bean.getType() == TypeConstant.INTEGER && !TypeID.LONG.equals(property.get_DataType())) 
			throw new AzException(ErrorCodeModule.AZM_FN_IM_PR02.extraMessage(property.get_DataType().toString()+" > "+TypeConstant.INTEGER_AS_STR));
		else if(bean.getType() == TypeConstant.DOUBLE && !TypeID.DOUBLE.equals(property.get_DataType())) 
			throw new AzException(ErrorCodeModule.AZM_FN_IM_PR02.extraMessage(property.get_DataType().toString()+" > "+TypeConstant.DOUBLE_AS_STR));
		else if(bean.getType() == TypeConstant.DATE && !TypeID.DATE.equals(property.get_DataType())) 
			throw new AzException(ErrorCodeModule.AZM_FN_IM_PR02.extraMessage(property.get_DataType().toString()+" > "+TypeConstant.DATE_AS_STR));
		else if(bean.getType() == TypeConstant.BOOLEAN && !TypeID.BOOLEAN.equals(property.get_DataType())) 
			throw new AzException(ErrorCodeModule.AZM_FN_IM_PR02.extraMessage(property.get_DataType().toString()+" > "+TypeConstant.BOOLEAN_AS_STR));
		else if(bean.getType() == TypeConstant.ID && !TypeID.GUID.equals(property.get_DataType())) 
			throw new AzException(ErrorCodeModule.AZM_FN_IM_PR02.extraMessage(property.get_DataType().toString()+" > "+TypeConstant.ID_AS_STR));
		else if(bean.getType() == TypeConstant.OBJECT && !TypeID.OBJECT.equals(property.get_DataType())) 
			throw new AzException(ErrorCodeModule.AZM_FN_IM_PR02.extraMessage(property.get_DataType().toString()+" > "+TypeConstant.OBJECT_AS_STR));
		else if(bean.getType() == TypeConstant.BINARY && !TypeID.BINARY.equals(property.get_DataType())) 
			throw new AzException(ErrorCodeModule.AZM_FN_IM_PR02.extraMessage(property.get_DataType().toString()+" > "+TypeConstant.BINARY_AS_STR));
		
		ImportResponse response = save(bean.getId(), property, bean, 
				updateMetadata, false, os, importBatch);
		return response;
	}
	
	@SuppressWarnings("unchecked")
	private static ImportResponse save(String id, PropertyTemplate impl, 
			AzProperty bean, boolean updateMetadata, boolean create, 
			ObjectStore os, UpdatingBatch importBatch) throws AzException {
		
		//Add properties
		impl.set_SymbolicName(bean.getIdName());
		
		LocalizedString locStr = Factory.LocalizedString.createInstance();
		locStr.set_LocalizedText(bean.getName());
		locStr.set_LocaleName (os.get_LocaleName());
		impl.set_DisplayNames (Factory.LocalizedString.createList());
		impl.get_DisplayNames().add(locStr);
		
		locStr = Factory.LocalizedString.createInstance();
		locStr.set_LocalizedText(bean.getDescription());
		locStr.set_LocaleName (os.get_LocaleName());
		impl.set_DescriptiveTexts(Factory.LocalizedString.createList());
		impl.get_DescriptiveTexts().add(locStr);
		
		impl.set_IsValueRequired(bean.isRequired());
		impl.set_IsHidden(bean.isHidden());
		impl.set_PropertyDisplayCategory(bean.getCategory());
		
		//Choice list
		if(bean.getOptionList() != null) {
			if(TypeConstant.STRING == bean.getType() || TypeConstant.INTEGER == bean.getType()) {
				impl.set_ChoiceList(Factory.ChoiceList.fetchInstance(os, new Id(bean.getOptionList().getId()), null));
			} else {
				throw new AzException(ErrorCodeModule.AZM_FN_IM_PR03.extraMessage("Type: "+TypeConstant.TEXT.get(bean.getType())));
			}
		}
		
		if(create) {
			//Cardinality
			if(!bean.isMultivalue()) {
				impl.set_Cardinality(Cardinality.SINGLE);
			} else {
				impl.set_Cardinality(Cardinality.LIST);
				if(CardinalityConstant.LIST == bean.getMultivalueCardinality()) 
					impl.set_RequiresUniqueElements(false);
				else if(CardinalityConstant.MAP == bean.getMultivalueCardinality()) 
					impl.set_RequiresUniqueElements(true);
			}
		} else {
			if(!bean.isMultivalue() && Cardinality.SINGLE != impl.get_Cardinality()) {
				throw new AzException(ErrorCodeModule.AZM_FN_IM_PR04.extraMessage(impl.get_Cardinality().toString()+" > Simple"));
			} else if(bean.isMultivalue() && bean.getMultivalueCardinality() == CardinalityConstant.LIST && 
					(Cardinality.LIST != impl.get_Cardinality() || impl.get_RequiresUniqueElements()) ) {
				throw new AzException(ErrorCodeModule.AZM_FN_IM_PR04.extraMessage(impl.get_Cardinality().toString()+" > "+CardinalityConstant.LIST_AS_STR));
			} else if(bean.isMultivalue() && bean.getMultivalueCardinality() == CardinalityConstant.MAP && 
					(Cardinality.LIST != impl.get_Cardinality() || !impl.get_RequiresUniqueElements()) ) {
				throw new AzException(ErrorCodeModule.AZM_FN_IM_PR04.extraMessage(impl.get_Cardinality().toString()+" > "+CardinalityConstant.MAP_AS_STR));
			}
		}
		
		//Specific for types
		switch(bean.getType()) {
			case TypeConstant.STRING:
				if(bean.getPropString() != null) {
					PropertyTemplateString implStr = (PropertyTemplateString) impl;
					implStr.set_MaximumLengthString(bean.getSize());
					implStr.set_PropertyDefaultString(bean.getPropString().getDefaultValue());
				}
				break;
			case TypeConstant.INTEGER: 
				if(bean.getPropInteger() != null) {
					PropertyTemplateInteger32 implInt = (PropertyTemplateInteger32) impl;
					implInt.set_PropertyDefaultInteger32(bean.getPropInteger().getDefaultValue());
					implInt.set_PropertyMinimumInteger32(bean.getPropInteger().getMin());
					implInt.set_PropertyMaximumInteger32(bean.getPropInteger().getMax());
				}
				break;
			case TypeConstant.DOUBLE: 
				if(bean.getPropDouble() != null) {
					PropertyTemplateFloat64 implFloat = (PropertyTemplateFloat64) impl;
					implFloat.set_PropertyDefaultFloat64(bean.getPropDouble().getDefaultValue());
					implFloat.set_PropertyMinimumFloat64(bean.getPropDouble().getMin());
					implFloat.set_PropertyMaximumFloat64(bean.getPropDouble().getMax());
				
				}
				break;
			case TypeConstant.DATE: 
				if(bean.getPropDate() != null) {
					PropertyTemplateDateTime implDate = (PropertyTemplateDateTime) impl;
					implDate.set_PropertyDefaultDateTime(bean.getPropDate().getDefaultValue());
					implDate.set_PropertyMinimumDateTime(bean.getPropDate().getMin());
					implDate.set_PropertyMaximumDateTime(bean.getPropDate().getMax());
				}
				break;
			case TypeConstant.BOOLEAN: 
				if(bean.getPropBoolean() != null) {
					PropertyTemplateBoolean implBoolean = (PropertyTemplateBoolean) impl;
					implBoolean.set_PropertyDefaultBoolean(bean.getPropBoolean().getDefaultValue());
				}
				break;
			case TypeConstant.ID: 
				if(bean.getPropId() != null) {
					PropertyTemplateId implId = (PropertyTemplateId) impl;
					if(bean.getPropId().getDefaultValue() != null && !bean.getPropId().getDefaultValue().trim().isEmpty())
						implId.set_PropertyDefaultId(new Id(bean.getPropId().getDefaultValue()));
				}
				break;
			case TypeConstant.OBJECT: 
				break;
			case TypeConstant.BINARY: 
				if(bean.getPropBinary() != null) {
					PropertyTemplateBinary implBinary = (PropertyTemplateBinary) impl;
					implBinary.set_MaximumLengthBinary(bean.getSize());
					implBinary.set_PropertyDefaultBinary(bean.getPropBinary().getDefaultValue());
				}
				break;
		}
		
		if(updateMetadata) {
			if(create) {
				impl.set_Creator(bean.getCreator());
				impl.set_DateCreated(bean.getCreated());
			}
			impl.set_LastModifier(bean.getModifier());
			impl.set_DateLastModified(bean.getModified());
		}
		
		
		//Save and add to internal
		importBatch.add(impl, null);
		
		ImportResponse response = new ImportResponse();
		response.setId(id);
		response.setIdName(bean.getIdName());
		response.setName(bean.getName());
		return response;
	}
}
