package cat.moon.azura.module.filenet.service;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import org.apache.log4j.Logger;

import com.filenet.api.admin.ClassDefinition;
import com.filenet.api.admin.LocalizedString;
import com.filenet.api.admin.PropertyDefinition;
import com.filenet.api.admin.PropertyTemplate;
import com.filenet.api.collection.IndependentObjectSet;
import com.filenet.api.collection.PropertyDefinitionList;
import com.filenet.api.constants.ClassNames;
import com.filenet.api.constants.PropertyNames;
import com.filenet.api.core.Factory;
import com.filenet.api.core.ObjectStore;
import com.filenet.api.core.UpdatingBatch;
import com.filenet.api.query.SearchSQL;
import com.filenet.api.query.SearchScope;
import com.filenet.api.util.Id;

import cat.moon.azura.bean.AzDocumentClass;
import cat.moon.azura.bean.AzProperty;
import cat.moon.azura.bean.generic.AzList;
import cat.moon.azura.bean.response.ImportResponse;
import cat.moon.azura.exception.AzDocumentClassParentNotFoundException;
import cat.moon.azura.exception.AzException;
import cat.moon.azura.module.filenet.util.IdUtil;
import cat.moon.azura.module.filenet.util.SQLUtils;

public class FNDocumentClassService {
	private static Logger logger = Logger.getLogger(FNDocumentClassService.class);
	
	public static List<AzList> list(ObjectStore os, int pageSize) {
		List<AzList> list = new ArrayList<AzList>();
		String sql = SQLUtils.createQuery(ClassNames.DOCUMENT_CLASS_DEFINITION, null, 
				PropertyNames.SYMBOLIC_NAME+" ASC");
		
		SearchScope search = new SearchScope(os);
		SearchSQL searchSQL = new SearchSQL(sql);
		IndependentObjectSet set = search.fetchObjects(searchSQL, pageSize, null, true);
		if(!set.isEmpty()) {
			@SuppressWarnings("unchecked")
			Iterator<ClassDefinition> it = set.iterator();
			AzList l;
			ClassDefinition impl;
			while(it.hasNext()) {
				impl = it.next();
				l = new AzList();
				l.setId(IdUtil.toString(impl.get_Id()));
				l.setIdName(impl.get_SymbolicName());
				l.setName(impl.get_Name());
				list.add(l);
			}
		} else logger.info("Nothing found");
		return list;
	}
	
	public static Map<String, Date> listId(ObjectStore os, int pageSize) {
		Map<String, Date> list = new HashMap<String, Date>();
		String sql = SQLUtils.createQuery(ClassNames.DOCUMENT_CLASS_DEFINITION, null, null);
		
		SearchScope search = new SearchScope(os);
		SearchSQL searchSQL = new SearchSQL(sql);
		IndependentObjectSet set = search.fetchObjects(searchSQL, pageSize, null, true);
		if(!set.isEmpty()) {
			@SuppressWarnings("unchecked")
			Iterator<ClassDefinition> it = set.iterator();
			ClassDefinition impl;
			while(it.hasNext()) {
				impl = it.next();
				list.put(impl.get_SymbolicName(), impl.get_DateLastModified());
			}
		}
		return list;
	}
	
	public static List<AzDocumentClass> get(ObjectStore os, int pageSize, List<String> ids) {
		List<AzDocumentClass> list = new ArrayList<AzDocumentClass>();
		
		String sql = SQLUtils.createQuery(ClassNames.DOCUMENT_CLASS_DEFINITION, ids);
		
		SearchScope search = new SearchScope(os);
		SearchSQL searchSQL = new SearchSQL(sql);
		IndependentObjectSet set = search.fetchObjects(searchSQL, pageSize, null, true);
		if(!set.isEmpty()) {
			@SuppressWarnings("unchecked")
			Iterator<ClassDefinition> it = set.iterator();
			while(it.hasNext()) {
				list.add(getDocumentClass(it.next()));
			}
		} else logger.info("Nothing found");;
		return list;
	}
	
	public static AzDocumentClass getDocumentClass(ClassDefinition impl) {
		if(logger.isDebugEnabled()) logger.debug("Id: "+impl.get_Id().toString()+" / Name: "+impl.get_Name());
		AzDocumentClass value = new AzDocumentClass();
		value.setId(IdUtil.toString(impl.get_Id()));
		value.setIdName(impl.get_SymbolicName());
		value.setName(impl.get_Name());
		value.setDescription(impl.get_DescriptiveText());
		value.setCreator(impl.get_Creator());
		value.setCreated(impl.get_DateCreated());
		value.setModifier(impl.get_LastModifier());
		value.setModified(impl.get_DateLastModified());
		
		if(impl.get_SuperclassDefinition() != null) {
			value.setIdParent(IdUtil.toString(impl.get_SuperclassDefinition().get_Id()));
		}
		
		if(!impl.get_PropertyDefinitions().isEmpty()) {
			value.setProperties(getProperties(impl));
			if(logger.isDebugEnabled()) logger.debug("Properties: "+value.getProperties().size());
		} else if(logger.isDebugEnabled()) logger.debug("Without properties");
		
		return value;
	}
	
	public static List<AzProperty> getProperties(ClassDefinition impl) {
		List<AzProperty> templates = new ArrayList<AzProperty>();
		
		//Get property template without the inherit properties
		PropertyDefinitionList pdl = impl.get_PropertyDefinitions();
		Integer inheritPropertyCount = impl.get_ProtectedPropertyCount();
		PropertyDefinition pd;
		for(int i = inheritPropertyCount; i<pdl.size(); i++) {
			pd = (PropertyDefinition) pdl.get(i);
			templates.add(FNPropertyService.getProperty(pd.get_PropertyTemplate()));
		}
		return templates;
	}
	
	public static ImportResponse add(AzDocumentClass bean, boolean updateMetadata, 
			ObjectStore os, UpdatingBatch importBatch, String documentClassDefaultId, 
			int propertyPageSize) throws AzException, AzDocumentClassParentNotFoundException {
		
		Id id;
		if(bean.getId() == null || bean.getId().trim().isEmpty()) id = Id.createId();
		else id = new Id(bean.getId());
		
		if(bean.getIdParent() == null || bean.getIdParent().trim().isEmpty()) bean.setIdParent(documentClassDefaultId);
		
		ClassDefinition documentClassParent = null;
		try {
			documentClassParent = Factory.ClassDefinition.fetchInstance(os, new Id(bean.getIdParent()), null);
		} catch(Exception e) {
			throw new AzDocumentClassParentNotFoundException();
		}
		if(documentClassParent != null) {
			ClassDefinition documentClass = documentClassParent.createSubclass(id);
			ImportResponse response = save(IdUtil.toString(id), documentClass, bean, 
					updateMetadata, true, os, importBatch, propertyPageSize);
			return response;
		} else throw new AzDocumentClassParentNotFoundException();
	}
	
	public static ImportResponse update(AzDocumentClass bean, boolean updateMetadata, 
			ObjectStore os, UpdatingBatch importBatch, String documentClassDefaultId,
			int propertyPageSize) throws AzException {
		
		ClassDefinition documentClass = Factory.ClassDefinition.fetchInstance(os, new Id(bean.getId()), null);
		
		ImportResponse response = save(bean.getId(), documentClass, bean, 
				updateMetadata, false, os, importBatch, propertyPageSize);
		return response;
	}
	
	@SuppressWarnings("unchecked")
	private static ImportResponse save(String id, ClassDefinition impl, 
			AzDocumentClass bean, boolean updateMetadata, boolean create, 
			ObjectStore os, UpdatingBatch importBatch, int propertyPageSize) throws AzException {
		
		//Add properties
		impl.set_SymbolicName(bean.getIdName());
		
		LocalizedString locStr = Factory.LocalizedString.createInstance();
		locStr.set_LocalizedText(bean.getName());
		locStr.set_LocaleName (os.get_LocaleName());
		impl.set_DisplayNames (Factory.LocalizedString.createList());
		impl.get_DisplayNames().add(locStr);
		
		locStr = Factory.LocalizedString.createInstance();
		locStr.set_LocalizedText(bean.getDescription());
		locStr.set_LocaleName (os.get_LocaleName());
		impl.set_DescriptiveTexts(Factory.LocalizedString.createList());
		impl.get_DescriptiveTexts().add(locStr);
		
		if(updateMetadata) {
			if(create) {
				impl.set_Creator(bean.getCreator());
				impl.set_DateCreated(bean.getCreated());
			}
			impl.set_LastModifier(bean.getModifier());
			impl.set_DateLastModified(bean.getModified());
		}
		
		//Add properties
		if(bean.getProperties() != null && !bean.getProperties().isEmpty()) {
			//Retrieve all symbolic names
			List<String> symbolicNames = new ArrayList<String>();
			for (AzProperty property : bean.getProperties()) {
				symbolicNames.add(property.getIdName());
			}
			PropertyDefinitionList pdl = impl.get_PropertyDefinitions();
			
			if(!create) {
				//Delete existing ids and properties that are no longer required
				Integer inheritPropertyCount = impl.get_ProtectedPropertyCount();
				PropertyDefinition pd;
				List<Integer> notRequired = new ArrayList<Integer>();
				for(int i = inheritPropertyCount; i<pdl.size(); i++) {
					pd = (PropertyDefinition) pdl.get(i);
					if(symbolicNames.contains(pd.get_SymbolicName())) {
						symbolicNames.remove(pd.get_SymbolicName());
					} else notRequired.add(i);
				}
				
				if(!notRequired.isEmpty()) {
					Collections.reverse(notRequired);
					for (Integer i : notRequired) pdl.remove(i.intValue());
				}
			}
			
			//Search and add new properties
			if(!symbolicNames.isEmpty()) {
				Iterator<PropertyTemplate> pTemplates = FNPropertyService.getPropertyDefinitionList(
						os, propertyPageSize, symbolicNames);
				
				if(pTemplates != null) {
					PropertyTemplate pTemplate;
					while(pTemplates.hasNext()) {
						pTemplate = pTemplates.next();
						pdl.add(pTemplate.createClassProperty());
					}
				}
			}
		}
		
		//Save
		importBatch.add(impl, null);
		
		ImportResponse response = new ImportResponse();
		response.setId(id);
		response.setIdName(bean.getIdName());
		response.setName(bean.getName());
		return response;
	}
}
