package cat.moon.azura.module.filenet.util;

import com.filenet.api.util.Id;

public class IdUtil {
	public static String toString(Id id) {
		return id.toString().substring(1, id.toString().length()-1);
	}
}
