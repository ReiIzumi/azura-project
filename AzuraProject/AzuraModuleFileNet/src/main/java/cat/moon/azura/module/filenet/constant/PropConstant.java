package cat.moon.azura.module.filenet.constant;

public class PropConstant {
	public static final String FN_URL						= "fn.url";
	public static final String FN_USER						= "fn.user";
	public static final String FN_PASSWORD					= "fn.password";
	public static final String FN_OBJECTSTORE				= "fn.objectstore";
	public static final String FN_JAASSTANZANAME			= "fn.jaasStanzaName";

	public static final String FN_DOCUMENTCLASS_PAGESIZE	= "fn.documentClass.pagesize";
	public static final String FN_PROPERTY_PAGESIZE			= "fn.property.pagesize";
	public static final String FN_CHOICELIST_PAGESIZE		= "fn.choiceList.pagesize";
	
	public static final String FN_DOCUMENTCLASS_ID			= "fn.documentClass.id";
	public static final String FN_PROPERTY_ID				= "fn.property.id";
	public static final String FN_CHOICELIST_ID				= "fn.choiceList.id";
	
	public static final String FN_DOCUMENTCLASS_DEFAULTID	= "fn.documentClass.defaultId";
}
