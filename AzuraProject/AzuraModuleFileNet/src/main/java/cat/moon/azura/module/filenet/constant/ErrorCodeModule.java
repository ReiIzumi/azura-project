package cat.moon.azura.module.filenet.constant;

import cat.moon.azura.bean.generic.ErrorMessage;

public class ErrorCodeModule {
	public static final ErrorMessage AZM_FN_01		= new ErrorMessage("AZM_FN_01", "Cannot connect to FileNet");
	
	public static final ErrorMessage AZM_FN_IM_OL01	= new ErrorMessage("AZM_FN_IM_OL01", "The DataType (Integer) from Choice List doesn't match with new String Type, this field cannot be updated");
	public static final ErrorMessage AZM_FN_IM_OL02	= new ErrorMessage("AZM_FN_IM_OL02", "The DataType (String) from Choice List doesn't match with new Integer Type, this field cannot be updated");
	
	public static final ErrorMessage AZM_FN_IM_PR01	= new ErrorMessage("AZM_FN_IM_PRO1", "Property type not found");
	public static final ErrorMessage AZM_FN_IM_PR02	= new ErrorMessage("AZM_FN_IM_PRO2", "The Property Type doesn't match with new Property Type, type cannot be updated. Please remove it before.");
	public static final ErrorMessage AZM_FN_IM_PR03	= new ErrorMessage("AZM_FN_IM_PRO3", "Only Option List is allowed for types String and Integer");
	public static final ErrorMessage AZM_FN_IM_PR04	= new ErrorMessage("AZM_FN_IM_PRO4", "Cardinality cannot be modified");
	
}
