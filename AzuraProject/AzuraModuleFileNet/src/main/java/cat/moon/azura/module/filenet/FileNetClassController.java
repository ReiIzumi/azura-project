package cat.moon.azura.module.filenet;

import java.util.Date;
import java.util.List;
import java.util.Map;

import org.apache.log4j.Logger;

import com.filenet.api.constants.RefreshMode;
import com.filenet.api.core.ObjectStore;
import com.filenet.api.core.UpdatingBatch;

import cat.moon.azura.bean.AzDocumentClass;
import cat.moon.azura.bean.AzOptionList;
import cat.moon.azura.bean.AzProperty;
import cat.moon.azura.bean.generic.AzList;
import cat.moon.azura.bean.response.ImportResponse;
import cat.moon.azura.constant.ImportIdConstant;
import cat.moon.azura.exception.AzDocumentClassParentNotFoundException;
import cat.moon.azura.exception.AzException;
import cat.moon.azura.module.IClassModule;
import cat.moon.azura.module.filenet.connection.FNConnection;
import cat.moon.azura.module.filenet.constant.ErrorCodeModule;
import cat.moon.azura.module.filenet.constant.PropConstant;
import cat.moon.azura.module.filenet.constant.Version;
import cat.moon.azura.module.filenet.service.FNChoiceListService;
import cat.moon.azura.module.filenet.service.FNDocumentClassService;
import cat.moon.azura.module.filenet.service.FNPropertyService;
import cat.moon.azura.util.PropUtil;

public class FileNetClassController implements IClassModule {
	private static Logger logger = Logger.getLogger(FileNetClassController.class);
	
	private static final int DEFAULT_PAGESIZE	= 50;
	private static final String DEFAULT_IMPORT_DOCUMENTCLASS_ID	= ImportIdConstant.ID_NAME;
	private static final String DEFAULT_IMPORT_PROPERTY_ID		= ImportIdConstant.ID_NAME;
	private static final String DEFAULT_IMPORT_CHOICELIST_ID	= ImportIdConstant.ID;
	private static final String DEFAULT_DOCUMENTCLASS_PARENTID	= "01A3A8C2-7AEC-11D1-A31B-0020AF9FBB1C";
	
	private ObjectStore os;
	private int choiceListPageSize;
	private int propertyPageSize;
	private int documentClassPageSize;
	
	private String documentClassId;
	private String propertyId;
	private String choiceListId;
	private String documentClassDefaultId;
	
	private UpdatingBatch importBatch;
	
	public void init(PropUtil prop, PropUtil moduleProp) throws AzException {
		logger.info("> FileNet Class Controller (v."+Version.VERSION+") <");
		
		FNConnection fnConnection = new FNConnection(
				moduleProp.get(PropConstant.FN_URL), 
				moduleProp.get(PropConstant.FN_USER), 
				moduleProp.get(PropConstant.FN_PASSWORD), 
				moduleProp.get(PropConstant.FN_OBJECTSTORE), 
				moduleProp.get(PropConstant.FN_JAASSTANZANAME) );
		try {
			os = fnConnection.getConnection();
		} catch(Exception e) {
			throw new AzException(ErrorCodeModule.AZM_FN_01, e);
		}
		
		documentClassPageSize = moduleProp.getInt(PropConstant.FN_DOCUMENTCLASS_PAGESIZE, DEFAULT_PAGESIZE);
		propertyPageSize = moduleProp.getInt(PropConstant.FN_PROPERTY_PAGESIZE, DEFAULT_PAGESIZE);
		choiceListPageSize = moduleProp.getInt(PropConstant.FN_CHOICELIST_PAGESIZE, DEFAULT_PAGESIZE);
		
		documentClassId = moduleProp.get(PropConstant.FN_DOCUMENTCLASS_ID, DEFAULT_IMPORT_DOCUMENTCLASS_ID);
		propertyId = moduleProp.get(PropConstant.FN_PROPERTY_ID, DEFAULT_IMPORT_PROPERTY_ID);
		choiceListId = moduleProp.get(PropConstant.FN_CHOICELIST_ID, DEFAULT_IMPORT_CHOICELIST_ID);
		
		documentClassDefaultId = moduleProp.get(PropConstant.FN_DOCUMENTCLASS_DEFAULTID, DEFAULT_DOCUMENTCLASS_PARENTID);
	}
	
	public List<AzList> listDocumentClass() throws AzException {
		return FNDocumentClassService.list(os, documentClassPageSize);
	}
	public List<AzList> listProperty() throws AzException {
		return FNPropertyService.list(os, propertyPageSize);
	}
	public List<AzList> listOptionList() throws AzException {
		return FNChoiceListService.list(os, choiceListPageSize);
	}
	
	public List<AzDocumentClass> exportDocumentClass(List<String> ids) throws AzException {
		return FNDocumentClassService.get(os, documentClassPageSize, ids);
	}
	public List<AzProperty> exportProperty(List<String> ids) throws AzException {
		return FNPropertyService.get(os, propertyPageSize, ids);
	}
	public List<AzOptionList> exportOptionList(List<String> ids) throws AzException {
		return FNChoiceListService.get(os, choiceListPageSize, ids);
	}
	
	public String getImportDocumentClassId() throws AzException {
		return documentClassId;
	}
	public String getImportPropertyId() throws AzException {
		return propertyId;
	}
	public String getImportOptionListId() throws AzException {
		return choiceListId;
	}
	public Map<String, Date> listCurrentDocumentClass() throws AzException {
		return FNDocumentClassService.listId(os, documentClassPageSize);
	}
	public Map<String, Date> listCurrentProperties() throws AzException {
		return FNPropertyService.listId(os, propertyPageSize);
	}
	public Map<String, Date> listCurrentOptionLists() throws AzException {
		return FNChoiceListService.listId(os, choiceListPageSize);
	}
	
	public ImportResponse addDocumentClass(AzDocumentClass bean, boolean updateMetadata) throws AzException, AzDocumentClassParentNotFoundException {
		return FNDocumentClassService.add(bean, updateMetadata, os, importBatch, documentClassDefaultId, propertyPageSize);
	}
	public ImportResponse addProperty(AzProperty bean, boolean updateMetadata) throws AzException {
		return FNPropertyService.add(bean, updateMetadata, os, importBatch);
	}
	public ImportResponse addOptionList(AzOptionList bean, boolean updateMetadata) throws AzException {
		return FNChoiceListService.add(bean, updateMetadata, os, importBatch);
	}
	public ImportResponse updateDocumentClass(AzDocumentClass bean, boolean updateMetadata) throws AzException {
		return FNDocumentClassService.update(bean, updateMetadata, os, importBatch, documentClassDefaultId, propertyPageSize);
	}
	public ImportResponse updateProperty(AzProperty bean, boolean updateMetadata) throws AzException {
		return FNPropertyService.update(bean, updateMetadata, os, importBatch);
	}
	public ImportResponse updateOptionList(AzOptionList bean, boolean updateMetadata) throws AzException {
		return FNChoiceListService.update(bean, updateMetadata, os, importBatch);
	}
	
	public void importPrepare() throws AzException {
		importBatch = UpdatingBatch.createUpdatingBatchInstance(os.get_Domain(), RefreshMode.REFRESH);
	}
	
	public void importSave() throws AzException {
		if(importBatch.hasPendingExecute()) importBatch.updateBatch();
		importBatch = null;
	}
	
	public void close() {}
}
