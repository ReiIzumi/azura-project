package cat.moon.azura.module.filenet.service;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import org.apache.log4j.Logger;

import com.filenet.api.admin.Choice;
import com.filenet.api.admin.ChoiceList;
import com.filenet.api.collection.IndependentObjectSet;
import com.filenet.api.constants.ChoiceType;
import com.filenet.api.constants.ClassNames;
import com.filenet.api.constants.PropertyNames;
import com.filenet.api.constants.TypeID;
import com.filenet.api.core.Factory;
import com.filenet.api.core.ObjectStore;
import com.filenet.api.core.UpdatingBatch;
import com.filenet.api.query.SearchSQL;
import com.filenet.api.query.SearchScope;
import com.filenet.api.util.Id;

import cat.moon.azura.bean.AzOptionList;
import cat.moon.azura.bean.AzOptionValue;
import cat.moon.azura.bean.generic.AzList;
import cat.moon.azura.bean.response.ImportResponse;
import cat.moon.azura.constant.TypeConstant;
import cat.moon.azura.exception.AzException;
import cat.moon.azura.module.filenet.constant.ErrorCodeModule;
import cat.moon.azura.module.filenet.util.IdUtil;
import cat.moon.azura.module.filenet.util.SQLUtils;

public class FNChoiceListService {
	private static Logger logger = Logger.getLogger(FNChoiceListService.class);
	
	public static List<AzList> list(ObjectStore os, int pageSize) {
		List<AzList> list = new ArrayList<AzList>();
		String sql = SQLUtils.createQuery(ClassNames.CHOICE_LIST, null, 
				PropertyNames.ID+" ASC");
		
		SearchScope search = new SearchScope(os);
		SearchSQL searchSQL = new SearchSQL(sql);
		IndependentObjectSet set = search.fetchObjects(searchSQL, pageSize, null, true);
		if(!set.isEmpty()) {
			@SuppressWarnings("unchecked")
			Iterator<ChoiceList> it = set.iterator();
			AzList l;
			ChoiceList impl;
			while(it.hasNext()) {
				impl = it.next();
				l = new AzList();
				l.setId(IdUtil.toString(impl.get_Id()));
				l.setName(impl.get_Name());
				list.add(l);
			}
		} else logger.info("Nothing found");;
		return list;
	}
	
	public static Map<String, Date> listId(ObjectStore os, int pageSize) {
		Map<String, Date> list = new HashMap<String, Date>();
		String sql = SQLUtils.createQuery(ClassNames.CHOICE_LIST, null, null);
		
		SearchScope search = new SearchScope(os);
		SearchSQL searchSQL = new SearchSQL(sql);
		IndependentObjectSet set = search.fetchObjects(searchSQL, pageSize, null, true);
		if(!set.isEmpty()) {
			@SuppressWarnings("unchecked")
			Iterator<ChoiceList> it = set.iterator();
			ChoiceList impl;
			while(it.hasNext()) {
				impl = it.next();
				list.put(IdUtil.toString(impl.get_Id()), impl.get_DateLastModified());
			}
		}
		return list;
	}
	
	public static List<AzOptionList> get(ObjectStore os, int pageSize, List<String> ids) {
		List<AzOptionList> list = new ArrayList<AzOptionList>();
		
		String sql = SQLUtils.createQuery(ClassNames.CHOICE_LIST, ids);
		
		SearchScope search = new SearchScope(os);
		SearchSQL searchSQL = new SearchSQL(sql);
		IndependentObjectSet set = search.fetchObjects(searchSQL, pageSize, null, true);
		if(!set.isEmpty()) {
			@SuppressWarnings("unchecked")
			Iterator<ChoiceList> it = set.iterator();
			while(it.hasNext()) {
				list.add(getOptionList(it.next()));
			}
		} else logger.info("Nothing found");
		return list;
	}
	
	public static AzOptionList getOptionList(ChoiceList impl) {
		if(logger.isDebugEnabled()) logger.debug("Id: "+impl.get_Id().toString()+" / Name: "+impl.get_Name());
		AzOptionList value = new AzOptionList();
		value.setId(IdUtil.toString(impl.get_Id()));
		value.setName(impl.get_Name());
		value.setDescription(impl.get_DescriptiveText());
		value.setCreator(impl.get_Creator());
		value.setCreated(impl.get_DateCreated());
		value.setModifier(impl.get_LastModifier());
		value.setModified(impl.get_DateLastModified());
		
		if(TypeID.STRING.equals(impl.get_DataType())) value.setType(TypeConstant.STRING);
		else if(TypeID.LONG.equals(impl.get_DataType())) value.setType(TypeConstant.INTEGER);
		
		com.filenet.api.collection.ChoiceList values = impl.get_ChoiceValues();
		if(!values.isEmpty()) {
			if(logger.isDebugEnabled()) logger.debug("Values founded: "+values.size());
			List<AzOptionValue> options  = new ArrayList<AzOptionValue>();
			
			@SuppressWarnings("unchecked")
			Iterator<Choice> itValues = values.iterator();
			AzOptionValue option;
			Choice choice;
			while(itValues.hasNext()) {
				choice = itValues.next();
				option = new AzOptionValue();
				option.setDisplayName(choice.get_DisplayName());
				if(ChoiceType.STRING.equals(choice.get_ChoiceType())) {
					option.setValueStr(choice.get_ChoiceStringValue());
					options.add(option);
				} else if(ChoiceType.INTEGER.equals(choice.get_ChoiceType())) {
					option.setValueInt(choice.get_ChoiceIntegerValue());
					options.add(option);
				} else if(ChoiceType.MIDNODE_STRING.equals(choice.get_ChoiceType())) {
					logger.warn("Midnode (String) is NOT compatible with Azura, it will be discarded. Id: "+choice.get_Id()+" / Name: "+choice.get_Name());
				} else if(ChoiceType.MIDNODE_INTEGER.equals(choice.get_ChoiceType())) {
					logger.warn("Midnode (Integer) is NOT compatible with Azura, it will be discarded. Id: "+choice.get_Id()+" / Name: "+choice.get_Name());
				}
			}
			value.setOptions(options);
		}
		return value;
	}
	
	public static ImportResponse add(AzOptionList bean, boolean updateMetadata, 
			ObjectStore os, UpdatingBatch importBatch) throws AzException {
		
		Id id;
		if(bean.getId() == null || bean.getId().trim().isEmpty()) id = Id.createId();
		else id = new Id(bean.getId());
		ChoiceList choiceList = Factory.ChoiceList.createInstance(os, id);
		
		ImportResponse response = save(IdUtil.toString(id), choiceList, bean, 
				updateMetadata, true, os, importBatch);
		return response;
	}
	
	public static ImportResponse update(AzOptionList bean, boolean updateMetadata, 
			ObjectStore os, UpdatingBatch importBatch) throws AzException {
		
		ChoiceList choiceList = Factory.ChoiceList.fetchInstance(os, new Id(bean.getId()), null);
		ImportResponse response = save(bean.getId(), choiceList, bean, 
				updateMetadata, false, os, importBatch);
		return response;
	}
	
	@SuppressWarnings("unchecked")
	private static ImportResponse save(String id, ChoiceList impl, 
			AzOptionList bean, boolean updateMetadata, boolean create, 
			ObjectStore os, UpdatingBatch importBatch) throws AzException {
		
		//Add properties
		impl.set_DisplayName(bean.getName());
		impl.set_DescriptiveText(bean.getDescription());
		
		if(create) {
			if(TypeConstant.STRING == bean.getType()) impl.set_DataType(TypeID.STRING);
			else if(TypeConstant.INTEGER == bean.getType()) impl.set_DataType(TypeID.LONG);
		} else {
			if(TypeConstant.STRING == bean.getType() && 
					impl.get_DataType().getValue() != TypeID.STRING.getValue()) {
				throw new AzException(ErrorCodeModule.AZM_FN_IM_OL01);
			} else if(TypeConstant.INTEGER == bean.getType() && 
					impl.get_DataType().getValue() != TypeID.LONG.getValue()) {
					throw new AzException(ErrorCodeModule.AZM_FN_IM_OL02);
				};
		}
		
		if(updateMetadata) {
			if(create) {
				impl.set_Creator(bean.getCreator());
				impl.set_DateCreated(bean.getCreated());
			}
			impl.set_LastModifier(bean.getModifier());
			impl.set_DateLastModified(bean.getModified());
		}
		
		//Add values
		if(bean.getOptions() != null && !bean.getOptions().isEmpty()) {
			com.filenet.api.collection.ChoiceList values = Factory.Choice.createList();
			
			Choice choice;
			for (AzOptionValue option : bean.getOptions()) {
				choice = Factory.Choice.createInstance(os);
				choice.set_DisplayName(option.getDisplayName());
				if(TypeConstant.STRING == bean.getType()) {
					choice.set_ChoiceType(ChoiceType.STRING);
					choice.set_ChoiceStringValue(option.getValueStr());
				} else if(TypeConstant.INTEGER == bean.getType()) {
					choice.set_ChoiceType(ChoiceType.INTEGER);
					choice.set_ChoiceIntegerValue(option.getValueInt());
				}
				values.add(choice);
			}
			impl.set_ChoiceValues(values);
		}
		
		//Save and add to internal
		importBatch.add(impl, null);
		
		ImportResponse response = new ImportResponse();
		response.setId(id);
		response.setName(bean.getName());
		return response;
	}
}
