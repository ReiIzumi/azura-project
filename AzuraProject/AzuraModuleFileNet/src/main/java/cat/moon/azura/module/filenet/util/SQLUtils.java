package cat.moon.azura.module.filenet.util;

import java.util.List;

import org.apache.log4j.Logger;

public class SQLUtils {
	private static Logger logger = Logger.getLogger(SQLUtils.class);
	
	public static String createQuery(String className, List<String> ids) {
		return createQuery(className, ids, null);
	}
	public static String createQuery(String className, List<String> ids, String orderBy) {
		StringBuilder sql = new StringBuilder();
		sql.append("SELECT * FROM "+className);
		SQLUtils.addWhereIdList(ids, sql);
		if(orderBy != null) sql.append(" ORDER BY "+orderBy);
		if(logger.isDebugEnabled()) logger.debug(sql.toString());
		return sql.toString();
	}
	
	public static String createQuerySymbolicName(String className, List<String> symbolicNames) {
		return createQuerySymbolicName(className, symbolicNames, null);
	}
	public static String createQuerySymbolicName(String className, List<String> symbolicNames, String orderBy) {
		StringBuilder sql = new StringBuilder();
		sql.append("SELECT * FROM "+className);
		SQLUtils.addWhereSymbolicNameList(symbolicNames, sql);
		if(orderBy != null) sql.append(" ORDER BY "+orderBy);
		if(logger.isDebugEnabled()) logger.debug(sql.toString());
		return sql.toString();
	}
	
	private static void addWhereIdList(List<String> ids, StringBuilder sql) {
		if(ids != null) {
			sql.append(" WHERE ");
			boolean first = true;
			for (String id : ids) {
				if(first) first = false;
				else sql.append(" OR ");
				sql.append("Id = '"+id+"'");
			}
		}
	}
	
	private static void addWhereSymbolicNameList(List<String> symbolicNames, StringBuilder sql) {
		if(symbolicNames != null) {
			sql.append(" WHERE ");
			boolean first = true;
			for (String symbolicName : symbolicNames) {
				if(first) first = false;
				else sql.append(" OR ");
				sql.append("SymbolicName = '"+symbolicName+"'");
			}
		}
	}
}
