package cat.moon.azura.module.filenet.connection;
 
import javax.security.auth.Subject;

import org.apache.log4j.Logger;

import com.filenet.api.core.Connection;
import com.filenet.api.core.Domain;
import com.filenet.api.core.Factory;
import com.filenet.api.core.ObjectStore;
import com.filenet.api.util.UserContext;

public class FNConnection {
	private final static Logger logger = Logger.getLogger(FNConnection.class);
	
	private String url;
	private String user;
	private String password;
	private String objectStore;
	private String jaasStanzaName;
	
	public FNConnection(String url, String user, String password, String objectStore, String jaasStanzaName) {
		this.url = url;
		this.user = user;
		this.password = password;
		this.objectStore = objectStore;
		this.jaasStanzaName = jaasStanzaName;
	}
	
	public ObjectStore getConnection() {
		if(logger.isDebugEnabled()) logger.debug("Connect to URL: "+url+" / ObjectStore: "+objectStore+" / User: "+user);
		Connection conn = Factory.Connection.getConnection(url);
		Subject ceSubject = UserContext.createSubject(conn, user, password, jaasStanzaName);
		UserContext.get().pushSubject(ceSubject);
		Domain domain = Factory.Domain.getInstance(conn, null);
		return Factory.ObjectStore.fetchInstance(domain, objectStore, null);
	}
}
