package cat.moon.azura.module.filenet.test;

import java.util.Properties;

import org.apache.log4j.Logger;
import org.apache.log4j.PropertyConfigurator;

import com.fasterxml.jackson.databind.ObjectMapper;

import cat.moon.azura.bean.AzDocumentClass;
import cat.moon.azura.bean.response.ImportResponse;
import cat.moon.azura.module.filenet.FileNetClassController;
import cat.moon.azura.util.PropUtil;

public class DocumentClassAddTest {
	private static Logger logger = Logger.getLogger(DocumentClassAddTest.class);
	
	public static void main(String[] args) throws Exception {
		try {
			FileNetClassController module = new FileNetClassController();
			Properties p = new Properties();
			p.load(DocumentClassAddTest.class.getResourceAsStream("/AzuraClass.properties"));
			Properties pM = new Properties();
			pM.load(DocumentClassAddTest.class.getResourceAsStream("/ModuleFileNet.properties"));
			PropUtil prop = new PropUtil(p);
			PropUtil moduleProp = new PropUtil(pM);
			PropertyConfigurator.configure(DocumentClassAddTest.class.getResourceAsStream("/log4j.properties"));
			
			ObjectMapper mapper = new ObjectMapper();
			AzDocumentClass bean = mapper.readValue(
					DocumentClassAddTest.class.getResourceAsStream("/Test_DocumentClass.json"), 
					AzDocumentClass.class);
			
			module.init(prop, moduleProp);
			module.importPrepare();
			ImportResponse importResponse = module.updateDocumentClass(bean , false);
			module.importSave();
			logger.info(importResponse);
		} finally {
			
		}
	}
}
