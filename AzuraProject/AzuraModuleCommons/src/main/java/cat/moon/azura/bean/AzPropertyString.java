package cat.moon.azura.bean;

import java.io.Serializable;

public class AzPropertyString implements Serializable {
	private static final long serialVersionUID = 788513496448500543L;
	
	private String defaultValue;
	
	public String getDefaultValue() {
		return defaultValue;
	}
	public void setDefaultValue(String defaultValue) {
		this.defaultValue = defaultValue;
	}
	
	@Override
	public String toString() {
		return "AzPropertyString [defaultValue=" + defaultValue + "]";
	}
}
