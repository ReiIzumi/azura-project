package cat.moon.azura.bean.response;

import java.io.Serializable;

public class ImportResponse implements Serializable {
	private static final long serialVersionUID = 8877704635419847125L;
	
	private String id;
	private String idName;
	private String name;
	
	public ImportResponse() {}
	public ImportResponse(String id, String idName, String name) {
		this.id = id;
		this.idName = idName;
		this.name = name;
	}

	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getIdName() {
		return idName;
	}
	public void setIdName(String idName) {
		this.idName = idName;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	
	public String toString() {
		return "ImportResponse [id=" + id + ", idName=" + idName + ", name=" + name + "]";
	}
}
