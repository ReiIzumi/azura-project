package cat.moon.azura.bean.generic;

import java.io.Serializable;

public class ErrorMessage implements Serializable {
	private static final long serialVersionUID = -8519684131131300546L;
	
	private static final int DEFAULT_EXITCODE	= 100;
	
	private int exitCode;
	private String code;
	private String message;
	
	public ErrorMessage() {
		this.exitCode = DEFAULT_EXITCODE;
	}
	
	public ErrorMessage(String code, String message) {
		this.exitCode = DEFAULT_EXITCODE;
		this.code = code;
		this.message = message;
	}
	public ErrorMessage(int exitCode, String code, String message) {
		this.exitCode = exitCode;
		this.code = code;
		this.message = message;
	}
	
	public int getExitCode() {
		return exitCode;
	}
	public void setExitCode(int exitCode) {
		this.exitCode = exitCode;
	}
	public String getCode() {
		return code;
	}
	public void setCode(String code) {
		this.code = code;
	}
	public String getMessage() {
		return message;
	}
	public void setMessage(String message) {
		this.message = message;
	}
	
	public ErrorMessage extraMessage(String extraMessage) {
		this.message = this.message+" - "+extraMessage;
		return this;
	}
	
	public String toString() {
		return code+" ("+exitCode+") - "+message;
	}
}
