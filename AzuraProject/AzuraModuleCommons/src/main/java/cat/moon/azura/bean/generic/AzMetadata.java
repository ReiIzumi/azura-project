package cat.moon.azura.bean.generic;

import java.io.Serializable;
import java.util.Date;

import com.fasterxml.jackson.annotation.JsonFormat;

public class AzMetadata implements Serializable {
	private static final long serialVersionUID = 7494942745977223812L;
	
	protected String id;
	protected String idName;
	protected String name;
	protected String description;
	
	protected String creator;
	@JsonFormat(shape=JsonFormat.Shape.STRING, pattern="yyyy-MM-dd_HH-mm-ss")
	protected Date created;
	protected String modifier;
	@JsonFormat(shape=JsonFormat.Shape.STRING, pattern="yyyy-MM-dd_HH-mm-ss")
	protected Date modified;
	
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getIdName() {
		return idName;
	}
	public void setIdName(String idName) {
		this.idName = idName;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	public String getCreator() {
		return creator;
	}
	public void setCreator(String creator) {
		this.creator = creator;
	}
	public Date getCreated() {
		return created;
	}
	public void setCreated(Date created) {
		this.created = created;
	}
	public String getModifier() {
		return modifier;
	}
	public void setModifier(String modifier) {
		this.modifier = modifier;
	}
	public Date getModified() {
		return modified;
	}
	public void setModified(Date modified) {
		this.modified = modified;
	}
	
	@Override
	public String toString() {
		return "AzMetadata [id=" + id + ", idName=" + idName + ", name=" + name + ", description=" + description
				+ ", creator=" + creator + ", created=" + created + ", modifier=" + modifier + ", modified=" + modified
				+ "]";
	}
}
