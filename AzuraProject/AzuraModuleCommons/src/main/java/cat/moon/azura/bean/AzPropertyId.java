package cat.moon.azura.bean;

import java.io.Serializable;

public class AzPropertyId implements Serializable {
	private static final long serialVersionUID = -3780577782899627693L;
	
	private String defaultValue;
	
	public String getDefaultValue() {
		return defaultValue;
	}
	public void setDefaultValue(String defaultValue) {
		this.defaultValue = defaultValue;
	}
	
	@Override
	public String toString() {
		return "AzPropertyId [defaultValue=" + defaultValue + "]";
	}
}
