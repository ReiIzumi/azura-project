package cat.moon.azura.bean;

import java.io.Serializable;
import java.util.Arrays;

public class AzPropertyBinary implements Serializable {
	private static final long serialVersionUID = -4017137630880547908L;
	
	private byte[] defaultValue;
	
	public byte[] getDefaultValue() {
		return defaultValue;
	}
	public void setDefaultValue(byte[] defaultValue) {
		this.defaultValue = defaultValue;
	}
	
	@Override
	public String toString() {
		return "AzPropertyBinary [defaultValue=" + Arrays.toString(defaultValue) + "]";
	}
}
