package cat.moon.azura.constant;

import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

public class CardinalityConstant {
	public static final int LIST				= 0;
	public static final int MAP					= 1;
	
	public static final String LIST_AS_STR		= "List";
	public static final String MAP_AS_STR		= "Map";
	
	public static final Map<Integer, String> TEXT;
	public static final Map<String, Integer> INT;
	
	static {
		Map<Integer, String> t = new HashMap<Integer, String>();
		t.put(LIST, LIST_AS_STR);
		t.put(MAP, MAP_AS_STR);
		TEXT = Collections.unmodifiableMap(t);
		
		Map<String, Integer> i = new HashMap<String, Integer>();
		i.put(LIST_AS_STR, LIST);
		i.put(MAP_AS_STR, MAP);
		INT = Collections.unmodifiableMap(i);
	}
}
