package cat.moon.azura.constant;

import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

public class TypeConstant {
	public static final int STRING				= 0;
	public static final int INTEGER				= 1;
	public static final int DOUBLE				= 2;
	public static final int DATE				= 3;
	public static final int BOOLEAN				= 4;
	public static final int ID					= 5;
	public static final int OBJECT				= 6;
	public static final int BINARY				= 7;
	
	public static final String STRING_AS_STR	= "String";
	public static final String INTEGER_AS_STR	= "Integer";
	public static final String DOUBLE_AS_STR	= "Double";
	public static final String DATE_AS_STR		= "Date";
	public static final String BOOLEAN_AS_STR	= "Boolean";
	public static final String ID_AS_STR		= "ID";
	public static final String OBJECT_AS_STR	= "Object";
	public static final String BINARY_AS_STR	= "Binary";
	
	public static final Map<Integer, String> TEXT;
	public static final Map<String, Integer> INT;
	
	static {
		Map<Integer, String> t = new HashMap<Integer, String>();
		t.put(STRING, STRING_AS_STR);
		t.put(INTEGER, INTEGER_AS_STR);
		t.put(DOUBLE, DOUBLE_AS_STR);
		t.put(DATE, DATE_AS_STR);
		t.put(BOOLEAN, BOOLEAN_AS_STR);
		t.put(ID, ID_AS_STR);
		t.put(OBJECT, OBJECT_AS_STR);
		t.put(BINARY, BINARY_AS_STR);
		TEXT = Collections.unmodifiableMap(t);
		
		Map<String, Integer> i = new HashMap<String, Integer>();
		i.put(STRING_AS_STR, STRING);
		i.put(INTEGER_AS_STR, INTEGER);
		i.put(DOUBLE_AS_STR, DOUBLE);
		i.put(DATE_AS_STR, DATE);
		i.put(BOOLEAN_AS_STR, BOOLEAN);
		i.put(ID_AS_STR, ID);
		i.put(OBJECT_AS_STR, OBJECT);
		i.put(BINARY_AS_STR, BINARY);
		INT = Collections.unmodifiableMap(i);
	}
}
