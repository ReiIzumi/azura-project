package cat.moon.azura.bean;

import java.io.Serializable;

import com.fasterxml.jackson.annotation.JsonIgnore;

public class AzOptionValue implements Serializable {
	private static final long serialVersionUID = -7453572293313274972L;
	
	private String displayName;
	private String valueStr;
	private Integer valueInt;
	@JsonIgnore
	private Object value;
	
	public String getDisplayName() {
		return displayName;
	}
	public void setDisplayName(String displayName) {
		this.displayName = displayName;
	}
	public String getValueStr() {
		return valueStr;
	}
	public void setValueStr(String valueStr) {
		this.valueStr = valueStr;
		this.value = valueStr;
	}
	public Integer getValueInt() {
		return valueInt;
	}
	public void setValueInt(Integer valueInt) {
		this.valueInt = valueInt;
		this.value = valueInt;
	}
	public Object getValue() {
		return value;
	}
	public void setValue(Object value) {
		this.value = value;
	}
	
	@Override
	public String toString() {
		return "AzOptionValue [displayName=" + displayName + ", valueStr=" + valueStr + ", valueInt=" + valueInt
				+ ", value=" + value + "]";
	}
}
