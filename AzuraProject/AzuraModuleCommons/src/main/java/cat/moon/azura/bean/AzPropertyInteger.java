package cat.moon.azura.bean;

import java.io.Serializable;

public class AzPropertyInteger implements Serializable {
	private static final long serialVersionUID = -671236477358225823L;
	
	private Integer defaultValue;
	private Integer max;
	private Integer min;
	
	public Integer getDefaultValue() {
		return defaultValue;
	}
	public void setDefaultValue(Integer defaultValue) {
		this.defaultValue = defaultValue;
	}
	public Integer getMax() {
		return max;
	}
	public void setMax(Integer max) {
		this.max = max;
	}
	public Integer getMin() {
		return min;
	}
	public void setMin(Integer min) {
		this.min = min;
	}
	
	@Override
	public String toString() {
		return "AzPropertyInteger [defaultValue=" + defaultValue + ", max=" + max + ", min=" + min + "]";
	}
}
