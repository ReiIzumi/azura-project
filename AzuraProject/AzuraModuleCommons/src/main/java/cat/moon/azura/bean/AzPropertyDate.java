package cat.moon.azura.bean;

import java.io.Serializable;
import java.util.Date;

public class AzPropertyDate implements Serializable {
	private static final long serialVersionUID = 1687135335598086108L;
	
	private Date defaultValue;
	private Date min;
	private Date max;
	
	public Date getDefaultValue() {
		return defaultValue;
	}
	public void setDefaultValue(Date defaultValue) {
		this.defaultValue = defaultValue;
	}
	public Date getMin() {
		return min;
	}
	public void setMin(Date min) {
		this.min = min;
	}
	public Date getMax() {
		return max;
	}
	public void setMax(Date max) {
		this.max = max;
	}
	
	@Override
	public String toString() {
		return "AzPropertyDate [defaultValue=" + defaultValue + ", min=" + min + ", max=" + max + "]";
	}
	
}
