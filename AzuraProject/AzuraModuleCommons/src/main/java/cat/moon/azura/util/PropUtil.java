package cat.moon.azura.util;

import java.util.Properties;

public class PropUtil {
	private Properties prop;
	
	public PropUtil(Properties prop) {
		this.prop = prop;
	}
	
	public boolean contains(String key) {
		return this.prop.containsKey(key);
	}
	
	public String get(String key) {
		String value = this.prop.getProperty(key);
		if(value != null) value = value.trim();
		return value;
	}
	
	public String get(String key, String defaultValue) {
		String value = this.prop.getProperty(key);
		if(value != null && !value.isEmpty()) return value.trim();
		else return defaultValue;
	}
	
	public Integer getInt(String key) {
		String value = this.prop.getProperty(key);
		if(value != null) {
			value = value.trim();
			if(value.isEmpty()) return null;
			else return Integer.parseInt(value);
		}
		else return null;
	}
	
	public Integer getInt(String key, Integer defaultValue) {
		String value = this.prop.getProperty(key);
		if(value != null && !value.isEmpty()) {
			value = value.trim();
			return Integer.parseInt(value);
		}
		else return defaultValue;
	}
}
