package cat.moon.azura.module;

import java.util.Date;
import java.util.List;
import java.util.Map;

import cat.moon.azura.bean.AzDocumentClass;
import cat.moon.azura.bean.AzOptionList;
import cat.moon.azura.bean.AzProperty;
import cat.moon.azura.bean.generic.AzList;
import cat.moon.azura.bean.response.ImportResponse;
import cat.moon.azura.exception.AzDocumentClassParentNotFoundException;
import cat.moon.azura.exception.AzException;
import cat.moon.azura.util.PropUtil;

public interface IClassModule {
	public void init(PropUtil prop, PropUtil moduleProp) throws AzException;
	
	public List<AzList> listDocumentClass() throws AzException;
	public List<AzList> listProperty() throws AzException;
	public List<AzList> listOptionList() throws AzException;
	
	public List<AzDocumentClass> exportDocumentClass(List<String> ids) throws AzException;
	public List<AzProperty> exportProperty(List<String> ids) throws AzException;
	public List<AzOptionList> exportOptionList(List<String> ids) throws AzException;
	
	public String getImportDocumentClassId() throws AzException;
	public String getImportPropertyId() throws AzException;
	public String getImportOptionListId() throws AzException;
	public Map<String, Date> listCurrentDocumentClass() throws AzException;
	public Map<String, Date> listCurrentProperties() throws AzException;
	public Map<String, Date> listCurrentOptionLists() throws AzException;
	
	public ImportResponse addDocumentClass(AzDocumentClass bean, boolean updateMetadata) throws AzException, AzDocumentClassParentNotFoundException;
	public ImportResponse addProperty(AzProperty bean, boolean updateMetadata) throws AzException;
	public ImportResponse addOptionList(AzOptionList bean, boolean updateMetadata) throws AzException;
	public ImportResponse updateDocumentClass(AzDocumentClass bean, boolean updateMetadata) throws AzException;
	public ImportResponse updateProperty(AzProperty bean, boolean updateMetadata) throws AzException;
	public ImportResponse updateOptionList(AzOptionList bean, boolean updateMetadata) throws AzException;
	
	public void importPrepare() throws AzException;
	public void importSave() throws AzException;
	
	public void close();
}
