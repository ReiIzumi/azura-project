package cat.moon.azura.bean;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;

import cat.moon.azura.bean.generic.AzMetadata;
import cat.moon.azura.json.CardinalityDeserializer;
import cat.moon.azura.json.CardinalitySerializer;
import cat.moon.azura.json.TypeDeserializer;
import cat.moon.azura.json.TypeSerializer;

public class AzProperty extends AzMetadata {
	private static final long serialVersionUID = 4626193830934273360L;
	
	@JsonDeserialize(using=TypeDeserializer.class)
	@JsonSerialize(using=TypeSerializer.class)
	private int type;
	
	private boolean multivalue;
	@JsonDeserialize(using=CardinalityDeserializer.class)
	@JsonSerialize(using=CardinalitySerializer.class)
	private Integer multivalueCardinality;
	private AzOptionList optionList;
	
	private boolean required;
	private boolean hidden;
	private String category;
	
	private Integer size;
	
	private AzPropertyString propString;
	private AzPropertyInteger propInteger;
	private AzPropertyDouble propDouble;
	private AzPropertyDate propDate;
	private AzPropertyBoolean propBoolean;
	private AzPropertyId propId;
	private AzPropertyBinary propBinary;
	
	public int getType() {
		return type;
	}
	public void setType(int type) {
		this.type = type;
	}
	public boolean isMultivalue() {
		return multivalue;
	}
	public void setMultivalue(boolean multivalue) {
		this.multivalue = multivalue;
	}
	public Integer getMultivalueCardinality() {
		return multivalueCardinality;
	}
	public void setMultivalueCardinality(Integer multivalueCardinality) {
		this.multivalueCardinality = multivalueCardinality;
	}
	public AzOptionList getOptionList() {
		return optionList;
	}
	public void setOptionList(AzOptionList optionList) {
		this.optionList = optionList;
	}
	public boolean isRequired() {
		return required;
	}
	public void setRequired(boolean required) {
		this.required = required;
	}
	public boolean isHidden() {
		return hidden;
	}
	public void setHidden(boolean hidden) {
		this.hidden = hidden;
	}
	public String getCategory() {
		return category;
	}
	public void setCategory(String category) {
		this.category = category;
	}
	public Integer getSize() {
		return size;
	}
	public void setSize(Integer size) {
		this.size = size;
	}
	public AzPropertyString getPropString() {
		return propString;
	}
	public void setPropString(AzPropertyString propString) {
		this.propString = propString;
	}
	public AzPropertyInteger getPropInteger() {
		return propInteger;
	}
	public void setPropInteger(AzPropertyInteger propInteger) {
		this.propInteger = propInteger;
	}
	public AzPropertyDouble getPropDouble() {
		return propDouble;
	}
	public void setPropDouble(AzPropertyDouble propDouble) {
		this.propDouble = propDouble;
	}
	public AzPropertyDate getPropDate() {
		return propDate;
	}
	public void setPropDate(AzPropertyDate propDate) {
		this.propDate = propDate;
	}
	public AzPropertyBoolean getPropBoolean() {
		return propBoolean;
	}
	public void setPropBoolean(AzPropertyBoolean propBoolean) {
		this.propBoolean = propBoolean;
	}
	public AzPropertyId getPropId() {
		return propId;
	}
	public void setPropId(AzPropertyId propId) {
		this.propId = propId;
	}
	public AzPropertyBinary getPropBinary() {
		return propBinary;
	}
	public void setPropBinary(AzPropertyBinary propBinary) {
		this.propBinary = propBinary;
	}
	
	@Override
	public String toString() {
		return "AzProperty [type=" + type + ", multivalue=" + multivalue + ", multivalueCardinality="
				+ multivalueCardinality + ", optionList=" + optionList + ", required=" + required + ", hidden=" + hidden
				+ ", category=" + category + ", size=" + size + ", propString=" + propString + ", propInteger="
				+ propInteger + ", propDouble=" + propDouble + ", propDate=" + propDate + ", propBoolean=" + propBoolean
				+ ", propId=" + propId + ", propBinary=" + propBinary + ", id=" + id + ", idName=" + idName + ", name="
				+ name + ", description=" + description + ", creator=" + creator + ", created=" + created
				+ ", modifier=" + modifier + ", modified=" + modified + "]";
	}
}
