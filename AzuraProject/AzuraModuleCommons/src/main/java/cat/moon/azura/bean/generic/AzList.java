package cat.moon.azura.bean.generic;

import java.io.Serializable;

public class AzList implements Serializable {
	private static final long serialVersionUID = 3524993972558914556L;
	
	private String id;
	private String idName;
	private String name;
	
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getIdName() {
		return idName;
	}
	public void setIdName(String idName) {
		this.idName = idName;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	
	@Override
	public String toString() {
		return "AzList [id=" + id + ", idName=" + idName + ", name=" + name + "]";
	}
}
