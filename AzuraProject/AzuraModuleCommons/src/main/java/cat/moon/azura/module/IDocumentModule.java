package cat.moon.azura.module;

import cat.moon.azura.exception.AzException;
import cat.moon.azura.util.PropUtil;

public interface IDocumentModule {
	public void init(PropUtil prop, PropUtil moduleProp) throws AzException;
	
	public void close();
}
