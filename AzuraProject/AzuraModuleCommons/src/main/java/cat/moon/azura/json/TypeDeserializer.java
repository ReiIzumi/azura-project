package cat.moon.azura.json;

import java.io.IOException;

import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.deser.std.StdDeserializer;

import cat.moon.azura.constant.TypeConstant;

public class TypeDeserializer extends StdDeserializer<Integer>{
	private static final long serialVersionUID = 4427025580355529801L;
	
	public TypeDeserializer() {
		super(TypeDeserializer.class);
	}
	
	@Override
	public Integer deserialize(JsonParser jsonparser, DeserializationContext context)
			throws IOException, JsonProcessingException {
		
		if(TypeConstant.INT.containsKey(jsonparser.getText())) {
			return TypeConstant.INT.get(jsonparser.getText());
		} else return -1;
	}
}
