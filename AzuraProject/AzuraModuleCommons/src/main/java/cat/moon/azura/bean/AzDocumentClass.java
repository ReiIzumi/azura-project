package cat.moon.azura.bean;

import java.util.List;

import cat.moon.azura.bean.generic.AzMetadata;

public class AzDocumentClass extends AzMetadata {
	private static final long serialVersionUID = 4607881108125389241L;
	
	private String idParent;
	private List<AzProperty> properties;
	
	public String getIdParent() {
		return idParent;
	}
	public void setIdParent(String idParent) {
		this.idParent = idParent;
	}
	public List<AzProperty> getProperties() {
		return properties;
	}
	public void setProperties(List<AzProperty> properties) {
		this.properties = properties;
	}
	
	@Override
	public String toString() {
		return "AzDocumentClass [idParent=" + idParent + ", properties=" + properties + ", id=" + id + ", idName="
				+ idName + ", name=" + name + ", description=" + description + ", creator=" + creator + ", created="
				+ created + ", modifier=" + modifier + ", modified=" + modified + "]";
	}
}
