package cat.moon.azura.bean.generic;

import java.io.Serializable;

public class AzCardinality implements Serializable {
	private static final long serialVersionUID = -6661192527163430941L;
	
	private String type;
	private int value;
	
	public AzCardinality(String type, int value) {
		this.type = type;
		this.value = value;
	}
	
	public String getType() {
		return type;
	}
	public void setType(String type) {
		this.type = type;
	}
	public int getValue() {
		return value;
	}
	public void setValue(int value) {
		this.value = value;
	}
	
	@Override
	public String toString() {
		return "AzType [type=" + type + ", value=" + value + "]";
	}
}
