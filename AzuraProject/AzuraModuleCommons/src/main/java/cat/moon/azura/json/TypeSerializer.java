package cat.moon.azura.json;

import java.io.IOException;

import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.databind.SerializerProvider;
import com.fasterxml.jackson.databind.ser.std.StdSerializer;

import cat.moon.azura.constant.TypeConstant;

public class TypeSerializer extends StdSerializer<Integer>{
	private static final long serialVersionUID = -7021646016456849172L;
	
	public TypeSerializer() {
		super(TypeSerializer.class, false);
	}
	
	@Override
	public void serialize(Integer value, JsonGenerator generator, SerializerProvider provider) throws IOException {
		if(TypeConstant.TEXT.containsKey(value)) {
			generator.writeString(TypeConstant.TEXT.get(value));
		}
	}
}
