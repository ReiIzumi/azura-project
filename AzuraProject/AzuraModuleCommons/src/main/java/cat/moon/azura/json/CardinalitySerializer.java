package cat.moon.azura.json;

import java.io.IOException;

import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.databind.SerializerProvider;
import com.fasterxml.jackson.databind.ser.std.StdSerializer;

import cat.moon.azura.constant.CardinalityConstant;

public class CardinalitySerializer extends StdSerializer<Integer>{
	private static final long serialVersionUID = 4717372333411285978L;

	public CardinalitySerializer() {
		super(CardinalitySerializer.class, false);
	}
	
	@Override
	public void serialize(Integer value, JsonGenerator generator, SerializerProvider provider) throws IOException {
		if(CardinalityConstant.TEXT.containsKey(value)) {
			generator.writeString(CardinalityConstant.TEXT.get(value));
		}
	}
}
