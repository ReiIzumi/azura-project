package cat.moon.azura.bean;

import java.io.Serializable;

public class AzPropertyDouble implements Serializable {
	private static final long serialVersionUID = -4062485170729795050L;
	
	private Double defaultValue;
	private Double min;
	private Double max;
	
	public Double getDefaultValue() {
		return defaultValue;
	}
	public void setDefaultValue(Double defaultValue) {
		this.defaultValue = defaultValue;
	}
	public Double getMin() {
		return min;
	}
	public void setMin(Double min) {
		this.min = min;
	}
	public Double getMax() {
		return max;
	}
	public void setMax(Double max) {
		this.max = max;
	}
	
	@Override
	public String toString() {
		return "AzPropertyDouble [defaultValue=" + defaultValue + ", min=" + min + ", max=" + max + "]";
	}
}
