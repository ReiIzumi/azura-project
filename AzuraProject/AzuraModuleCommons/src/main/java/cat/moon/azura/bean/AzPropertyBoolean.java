package cat.moon.azura.bean;

import java.io.Serializable;

public class AzPropertyBoolean implements Serializable {
	private static final long serialVersionUID = -1362059467563800979L;
	
	private Boolean defaultValue;
	
	public Boolean getDefaultValue() {
		return defaultValue;
	}
	public void setDefaultValue(Boolean defaultValue) {
		this.defaultValue = defaultValue;
	}
	
	@Override
	public String toString() {
		return "AzPropertyBoolean [defaultValue=" + defaultValue + "]";
	}
}
