package cat.moon.azura.bean;

import java.util.List;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;

import cat.moon.azura.bean.generic.AzMetadata;
import cat.moon.azura.json.TypeDeserializer;
import cat.moon.azura.json.TypeSerializer;

public class AzOptionList extends AzMetadata {
	private static final long serialVersionUID = -2900993749052011388L;
	
	@JsonDeserialize(using=TypeDeserializer.class)
	@JsonSerialize(using=TypeSerializer.class)
	private int type;
	private List<AzOptionValue> options;
	
	public int getType() {
		return type;
	}
	public void setType(int type) {
		this.type = type;
	}
	public List<AzOptionValue> getOptions() {
		return options;
	}
	public void setOptions(List<AzOptionValue> options) {
		this.options = options;
	}
	
	@Override
	public String toString() {
		return "AzOptionList [type=" + type + ", options=" + options + ", id=" + id + ", idName=" + idName
				+ ", name=" + name + ", description=" + description + ", creator=" + creator + ", created=" + created
				+ ", modifier=" + modifier + ", modified=" + modified + "]";
	}
}
