package cat.moon.azura.exception;

import cat.moon.azura.bean.generic.ErrorMessage;

public class AzException extends Exception {
	private static final long serialVersionUID = -8364282955527518819L;
	
	protected ErrorMessage errorMessage;
	
	public AzException(ErrorMessage errorMessage) {
		super();
		this.errorMessage = errorMessage;
	}

	public AzException(ErrorMessage errorMessage, String message, Throwable cause) {
		super(message, cause);
		this.errorMessage = errorMessage;
	}

	public AzException(ErrorMessage errorMessage, String message) {
		super(message);
		this.errorMessage = errorMessage;
	}

	public AzException(ErrorMessage errorMessage, Throwable cause) {
		super(cause);
		this.errorMessage = errorMessage;
	}

	public ErrorMessage getErrorMessage() {
		return errorMessage;
	}
	public void setErrorMessage(ErrorMessage errorMessage) {
		this.errorMessage = errorMessage;
	}
	
	@Override
	public String toString() {
		return this.errorMessage.toString();
	}
}
