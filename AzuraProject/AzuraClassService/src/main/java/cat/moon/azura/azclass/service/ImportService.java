package cat.moon.azura.azclass.service;

import java.io.File;
import java.util.Collection;
import java.util.Date;
import java.util.Iterator;
import java.util.Map;

import org.apache.commons.io.FileUtils;
import org.apache.commons.io.FilenameUtils;
import org.apache.log4j.Logger;

import com.fasterxml.jackson.databind.ObjectMapper;

import cat.moon.azura.azclass.bean.ImportAction;
import cat.moon.azura.azclass.constant.ErrorCodeClass;
import cat.moon.azura.azclass.exception.ImportException;
import cat.moon.azura.azclass.exception.WarningException;
import cat.moon.azura.azclass.util.CurrentItemUtil;
import cat.moon.azura.bean.AzDocumentClass;
import cat.moon.azura.bean.AzOptionList;
import cat.moon.azura.bean.AzProperty;
import cat.moon.azura.bean.response.ImportResponse;
import cat.moon.azura.constant.ActionConstant;
import cat.moon.azura.constant.PropConstant;
import cat.moon.azura.constant.ServiceConstant;
import cat.moon.azura.exception.AzDocumentClassParentNotFoundException;
import cat.moon.azura.exception.AzException;
import cat.moon.azura.module.IClassModule;
import cat.moon.azura.util.FileUtil;
import cat.moon.azura.util.PropUtil;

public class ImportService {
	private static Logger logger = Logger.getLogger(ImportService.class);
	
	private static final String EXTENSION			= "json";
	private static final String EXTENSION_WORK		= "working";
	
	private String serviceName;
	private int serviceNameLength;
	
	private File sourceFolder;
	private String sourceFolderPath;
	private String errorFolder;
	private String processedFolder;
	
	private Collection<File> files;
	private Iterator<File> it;
	private ObjectMapper mapper;
	
	private String currentFilename;
	private String currentId;
	private File currentFile;
	
	private boolean documentClassAlwaysUpdate = false;
	private boolean documentClassUpdateNewer = false;
	private boolean propertyAlwaysUpdate = false;
	private boolean propertyUpdateNewer = false;
	private boolean optionListAlwaysUpdate = false;
	private boolean optionListUpdateNewer = false;
	
	public ImportService(PropUtil prop, String sourceFolder, String errorFolder, String processedFolder,
			String serviceName) throws AzException {
		
		logger.info("> Initializing Import Service");
		
		this.serviceName = serviceName;
		serviceNameLength = ExportService.FILENAME.length()+serviceName.length()+1;
		
		this.sourceFolder = FileUtil.getFolder(prop.get(sourceFolder));
		this.errorFolder = FileUtil.getFolderPath(prop.get(errorFolder));
		this.processedFolder = FileUtil.getFolderPath(prop.get(processedFolder));
		sourceFolderPath = this.sourceFolder.getAbsolutePath()+"\\";
		logger.info(">> Import source folder: "+sourceFolderPath);
		logger.info(">> Import error folder: "+this.errorFolder);
		logger.info(">> Import processed folder: "+this.processedFolder);
		
		mapper = new ObjectMapper();
		
		if("Y".equals(prop.get(PropConstant.IMPORT_DOCUMENTCLASS_ALWAYS_UPDATE))) 	documentClassAlwaysUpdate = true;
		else if("Y".equals(prop.get(PropConstant.IMPORT_DOCUMENTCLASS_UPDATE_NEWER)))documentClassUpdateNewer = true;
		if("Y".equals(prop.get(PropConstant.IMPORT_PROPERTY_ALWAYS_UPDATE)))		propertyAlwaysUpdate = true;
		else if("Y".equals(prop.get(PropConstant.IMPORT_PROPERTY_UPDATE_NEWER)))	propertyUpdateNewer = true;
		if("Y".equals(prop.get(PropConstant.IMPORT_OPTIONLIST_ALWAYS_UPDATE)))		optionListAlwaysUpdate = true;
		else if("Y".equals(prop.get(PropConstant.IMPORT_OPTIONLIST_UPDATE_NEWER)))	optionListUpdateNewer = true;
		
	}
	
	public int searchFiles() {
		files = FileUtils.listFiles(sourceFolder, new String[] {EXTENSION}, false);
		if(!files.isEmpty()) it = files.iterator();
		return files.size();
	}
	
	public File nextFile() {
		if(it.hasNext()) return it.next();
		else return null;
	}
	
	public AzOptionList prepareOptionList(File file) throws ImportException {
		AzOptionList bean = null;
		prepareCurrentFile(file);
		try {
			bean = mapper.readValue(currentFile, AzOptionList.class);
		} catch (Exception e) {
			ImportException ie = new ImportException(ErrorCodeClass.AZC_FI_03, e);
			ie.setFilename(file.getName());
			throw ie;
		}
		return bean;
	}
	public AzProperty prepareProperty(File file) throws ImportException {
		AzProperty bean = null;
		prepareCurrentFile(file);
		try {
			bean = mapper.readValue(currentFile, AzProperty.class);
		} catch (Exception e) {
			ImportException ie = new ImportException(ErrorCodeClass.AZC_FI_03, e);
			ie.setFilename(file.getName());
			throw ie;
		}
		return bean;
	}
	public AzDocumentClass prepareDocumentClass(File file) throws ImportException {
		AzDocumentClass bean = null;
		prepareCurrentFile(file);
		try {
			bean = mapper.readValue(currentFile, AzDocumentClass.class);
		} catch (Exception e) {
			ImportException ie = new ImportException(ErrorCodeClass.AZC_FI_03, e);
			ie.setFilename(file.getName());
			throw ie;
		}
		return bean;
	}
	
	public void currentFileProcessed() throws WarningException {
		try {
			File targetFile = new File(processedFolder+currentFilename);
			if(targetFile.isFile()) targetFile.delete();
			FileUtils.moveFile(currentFile, targetFile);
		} catch (Exception e) {
			WarningException we = new WarningException(ErrorCodeClass.AZC_FI_04, e);
			throw we;
		}
	}
	
	public void currentFileError() throws WarningException {
		try {
			File targetFile = new File(errorFolder+currentFilename);
			if(targetFile.isFile()) targetFile.delete();
			FileUtils.moveFile(currentFile, targetFile);
		} catch (Exception e) {
			WarningException we = new WarningException(ErrorCodeClass.AZC_FI_05, e);
			throw we;
		}
	}
	
	private void prepareCurrentFile(File file) throws ImportException {
		try {
			currentFilename = file.getName();
			currentFile = new File(sourceFolderPath+FilenameUtils.getBaseName(currentFilename)+"."+EXTENSION_WORK);
			logger.info("Importing "+currentFilename);
			
			if(currentFilename.startsWith(ExportService.FILENAME+serviceName)) {
				currentId = FilenameUtils.getBaseName(currentFilename).substring(serviceNameLength);
			} else currentId = FilenameUtils.getBaseName(currentFilename);
			if(logger.isDebugEnabled()) logger.debug(">> Id: "+currentId);
		} catch(Exception e) {
			ImportException ie = new ImportException(ErrorCodeClass.AZC_FI_01, e);
			ie.setFilename(file.getName());
			throw ie;
		}
		try {
			FileUtils.moveFile(file, currentFile);
		} catch (Exception e) {
			ImportException ie = new ImportException(ErrorCodeClass.AZC_FI_02, e);
			ie.setFilename(file.getName());
			throw ie;
		}
	}
	
	public int checkDocumentClass(AzDocumentClass bean, Date modified) {
		if(modified == null) return ActionConstant.CREATE;
		else {
			if(documentClassAlwaysUpdate) return ActionConstant.UPDATE_ALWAYS;
			else if(documentClassUpdateNewer && modified.after(bean.getModified())) 
				return ActionConstant.UPDATE_MODIFIED;
			else return ActionConstant.DISCARD;
		}
	}
	
	public int checkProperty(AzProperty bean, Date modified) {
		if(modified == null) return ActionConstant.CREATE;
		else {
			if(propertyAlwaysUpdate) return ActionConstant.UPDATE_ALWAYS;
			else if(propertyUpdateNewer && modified.after(bean.getModified())) 
				return ActionConstant.UPDATE_MODIFIED;
			else return ActionConstant.DISCARD;
		}
	}
	
	public int checkOptionList(AzOptionList bean, Date modified) {
		if(modified == null) return ActionConstant.CREATE;
		else {
			if(optionListAlwaysUpdate) return ActionConstant.UPDATE_ALWAYS;
			else if(optionListUpdateNewer && bean.getModified().getTime() > modified.getTime()) {
				return ActionConstant.UPDATE_MODIFIED;
			}
			else return ActionConstant.DISCARD;
		}
	}
	
	public ImportAction importDocumentClass(AzDocumentClass bean, String idField, Map<String, Date> current, 
			boolean updateMetadata, IClassModule module) throws AzException, AzDocumentClassParentNotFoundException {
		
		Date modified = CurrentItemUtil.getModifierDate(bean.getId(), bean.getIdName(), 
				idField, current);
		int action = checkDocumentClass(bean, modified);
		if(logger.isDebugEnabled()) logger.debug("Action: "+ActionConstant.TEXT.get(action));
		
		//Import items
		if(logger.isDebugEnabled()) logger.debug(ServiceConstant.DOCUMENT_CLASS+": "+bean.getId()+" / "+bean.getIdName()+" / "+bean.getName());
		ImportResponse importResponse = null;
		switch (action) {
			case ActionConstant.CREATE:
				importResponse = module.addDocumentClass(bean, updateMetadata);
				break;
			case ActionConstant.UPDATE_ALWAYS:
			case ActionConstant.UPDATE_MODIFIED:
				importResponse = module.updateDocumentClass(bean, updateMetadata);
				break;
			case ActionConstant.DISCARD:
				importResponse = new ImportResponse(bean.getId(), bean.getIdName(), bean.getName());
				break;
		}
		if(bean.getId() == null) bean.setId(importResponse.getId());
		return new ImportAction(action, ServiceConstant.DOCUMENT_CLASS, importResponse); 
	}
	
	public ImportAction importProperty(AzProperty bean, String idField, Map<String, Date> current, 
			boolean updateMetadata, IClassModule module) throws AzException {
		
		Date modified = CurrentItemUtil.getModifierDate(bean.getId(), bean.getIdName(), 
				idField, current);
		int action = checkProperty(bean, modified);
		if(logger.isDebugEnabled()) logger.debug("Action: "+ActionConstant.TEXT.get(action));
		
		//Import items
		if(logger.isDebugEnabled()) logger.debug(ServiceConstant.PROPERTY+": "+bean.getId()+" / "+bean.getIdName()+" / "+bean.getName());
		ImportResponse importResponse = null;
		switch (action) {
			case ActionConstant.CREATE:
				importResponse = module.addProperty(bean, updateMetadata);
				break;
			case ActionConstant.UPDATE_ALWAYS:
			case ActionConstant.UPDATE_MODIFIED:
				importResponse = module.updateProperty(bean, updateMetadata);
				break;
			case ActionConstant.DISCARD:
				importResponse = new ImportResponse(bean.getId(), bean.getIdName(), bean.getName());
				break;
		}
		if(bean.getId() == null) bean.setId(importResponse.getId());
		return new ImportAction(action, ServiceConstant.PROPERTY, importResponse); 
	}
	
	public ImportAction importOptionList(AzOptionList bean, String idField, Map<String, Date> current, 
			boolean updateMetadata, IClassModule module) throws AzException {
		
		Date modified = CurrentItemUtil.getModifierDate(bean.getId(), bean.getIdName(), 
				idField, current);
		int action = checkOptionList(bean, modified);
		if(logger.isDebugEnabled()) logger.debug("Action: "+ActionConstant.TEXT.get(action));
		
		//Import items
		if(logger.isDebugEnabled()) logger.debug(ServiceConstant.OPTION_LIST+": "+bean.getId()+" / "+bean.getIdName()+" / "+bean.getName());
		ImportResponse importResponse = null;
		switch (action) {
			case ActionConstant.CREATE:
				importResponse = module.addOptionList(bean, updateMetadata);
				break;
			case ActionConstant.UPDATE_ALWAYS:
			case ActionConstant.UPDATE_MODIFIED:
				importResponse = module.updateOptionList(bean, updateMetadata);
				break;
			case ActionConstant.DISCARD:
				importResponse = new ImportResponse(bean.getId(), bean.getIdName(), bean.getName());
				break;
		}
		if(bean.getId() == null) bean.setId(importResponse.getId());
		return new ImportAction(action, ServiceConstant.OPTION_LIST, importResponse); 
	}
}
