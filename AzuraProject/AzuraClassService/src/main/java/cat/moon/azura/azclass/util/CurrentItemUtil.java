package cat.moon.azura.azclass.util;

import java.util.Date;
import java.util.Map;

import cat.moon.azura.constant.ImportIdConstant;

public class CurrentItemUtil {
	public static String checkId(String id) {
		if(!id.equals(ImportIdConstant.ID) && !id.equals(ImportIdConstant.ID_NAME)) 
			return ImportIdConstant.ID;
		else return id;
	}
	
	public static Date getModifierDate(String id, String idName, String idProperty, Map<String, Date> current) {
		String idCheck = null;
		if(idProperty.equals(ImportIdConstant.ID)) idCheck = id;
		else idCheck = idName;
		
		if(current != null && current.containsKey(idCheck)) 
			return current.get(idCheck);
		else return null;
	}
}
