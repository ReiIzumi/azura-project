package cat.moon.azura.azclass.controller;

import java.util.List;

import org.apache.log4j.Logger;

import cat.moon.azura.azclass.constant.Version;
import cat.moon.azura.azclass.service.ExportService;
import cat.moon.azura.azclass.service.ReportExportService;
import cat.moon.azura.bean.AzDocumentClass;
import cat.moon.azura.constant.ErrorCodeCommons;
import cat.moon.azura.constant.PropConstant;
import cat.moon.azura.constant.ServiceConstant;
import cat.moon.azura.exception.AzException;
import cat.moon.azura.module.IClassModule;
import cat.moon.azura.service.ModuleService;
import cat.moon.azura.util.PropUtil;

public class ExportDocumentClassController {
	private static Logger logger = Logger.getLogger(ExportDocumentClassController.class);
	
	public static void start(PropUtil prop, List<String> ids) throws AzException {
		IClassModule module = null;
		try {
			logger.info("============================== ==============================");
			logger.info("Starting Azura Class Service (v."+Version.VERSION+")");
			logger.info("- Export Document Class (v."+Version.EXPORT_DOCUMENT_CLASS+")");
			logger.info("============================== ==============================");
			
			/* Start export and report service */
			ExportService service = new ExportService(prop, 
					PropConstant.CLASS_EXPORT_FOLDER, ServiceConstant.DOCUMENT_CLASS);
			ReportExportService reportService = new ReportExportService(prop, 
					PropConstant.REPORT_CLASS_EXPORT_FOLDER, ServiceConstant.DOCUMENT_CLASS);
			
			/* Start module */
			module = ModuleService.getClassModule(prop);
			
			/* Export from module */
			List<AzDocumentClass> list = module.exportDocumentClass(ids);
			
			/* Generate files and report */
			service.generateDocumentClass(list);
			reportService.generateDocumentClass(list);
			
		} catch(AzException e) {
			throw e;
		} catch(Exception e) {
			throw new AzException(ErrorCodeCommons.DEFAULT_ERROR, e);
		} finally {
			if(module != null) module.close();
		}
	}
}
