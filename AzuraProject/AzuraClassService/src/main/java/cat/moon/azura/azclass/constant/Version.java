package cat.moon.azura.azclass.constant;

public class Version {
	public static final String VERSION					= "1.0.1";
	public static final String LIST_DOCUMENT_CLASS		= "1.0.0";
	public static final String LIST_PROPERTIES			= "1.0.0";
	public static final String LIST_OPTIONLIST			= "1.0.0";
	public static final String EXPORT_DOCUMENT_CLASS	= "1.0.0";
	public static final String EXPORT_PROPERTIES		= "1.0.0";
	public static final String EXPORT_OPTIONLIST		= "1.0.0";
	public static final String IMPORT_DOCUMENT_CLASS	= "1.0.1";
	public static final String IMPORT_PROPERTIES		= "1.0.0";
	public static final String IMPORT_OPTIONLIST		= "1.0.0";
}
