package cat.moon.azura.azclass.bean;

import java.io.Serializable;

import cat.moon.azura.bean.response.ImportResponse;

public class ImportAction implements Serializable {
	private static final long serialVersionUID = 5654975275588429393L;
	
	private int action;
	private String service;
	private ImportResponse importResponse;
	private String fileName;
	
	public ImportAction() {}
	public ImportAction(int action, String service, ImportResponse importResponse) {
		this.action = action;
		this.service = service;
		this.importResponse = importResponse;
	}
	public ImportAction(int action, String service, ImportResponse importResponse, String fileName) {
		this.action = action;
		this.service = service;
		this.importResponse = importResponse;
		this.fileName = fileName;
	}
	
	public int getAction() {
		return action;
	}
	public void setAction(int action) {
		this.action = action;
	}
	public String getService() {
		return service;
	}
	public void setService(String service) {
		this.service = service;
	}
	public ImportResponse getImportResponse() {
		return importResponse;
	}
	public void setImportResponse(ImportResponse importResponse) {
		this.importResponse = importResponse;
	}
	public String getFileName() {
		return fileName;
	}
	public void setFileName(String fileName) {
		this.fileName = fileName;
	}
	
	@Override
	public String toString() {
		return "ImportAction [action=" + action + ", service=" + service + ", importResponse=" + importResponse
				+ ", fileName=" + fileName + "]";
	}
}
