package cat.moon.azura.azclass.util;

import java.io.File;

import cat.moon.azura.azclass.exception.ImportException;
import cat.moon.azura.bean.AzDocumentClass;
import cat.moon.azura.bean.AzOptionList;
import cat.moon.azura.bean.AzProperty;

public class ErrorUtil {
	private static void define(ImportException e, File file) {
		if(e.getFilename() == null) e.setFilename(file.getName());
		
	}
	
	public static void define(ImportException e, File file, AzDocumentClass bean) {
		define(e, file);
		if(bean != null) {
			if(e.getId() == null) e.setId(bean.getId());
			if(e.getIdName() == null) e.setIdName(bean.getIdName());
			if(e.getName() == null) e.setName(bean.getName());
		}
	}
	
	public static void define(ImportException e, File file, AzProperty bean) {
		define(e, file);
		if(bean != null) {
			if(e.getId() == null) e.setId(bean.getId());
			if(e.getIdName() == null) e.setIdName(bean.getIdName());
			if(e.getName() == null) e.setName(bean.getName());
		}
	}
	
	public static void define(ImportException e, File file, AzOptionList bean) {
		define(e, file);
		if(bean != null) {
			if(e.getId() == null) e.setId(bean.getId());
			if(e.getIdName() == null) e.setIdName(bean.getIdName());
			if(e.getName() == null) e.setName(bean.getName());
		}
	}
}
