package cat.moon.azura.azclass.exception;

import cat.moon.azura.bean.generic.ErrorMessage;
import cat.moon.azura.exception.AzException;

public class ImportException extends AzException {
	private static final long serialVersionUID = 6725622649139905265L;
	
	private String filename;
	private String id;
	private String idName;
	private String name;
	
	public ImportException(ErrorMessage errorMessage) {
		super(errorMessage);
	}
	public ImportException(ErrorMessage errorMessage, String message, Throwable cause) {
		super(errorMessage, message, cause);
	}
	public ImportException(ErrorMessage errorMessage, String message) {
		super(errorMessage, message);
	}
	public ImportException(ErrorMessage errorMessage, Throwable cause) {
		super(errorMessage, cause);
	}
	
	public String getFilename() {
		return filename;
	}
	public void setFilename(String filename) {
		this.filename = filename;
	}
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getIdName() {
		return idName;
	}
	public void setIdName(String idName) {
		this.idName = idName;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
}
