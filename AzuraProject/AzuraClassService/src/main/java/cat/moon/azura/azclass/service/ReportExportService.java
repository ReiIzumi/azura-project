package cat.moon.azura.azclass.service;

import java.util.List;

import org.apache.log4j.Logger;

import cat.moon.azura.bean.AzDocumentClass;
import cat.moon.azura.bean.AzOptionList;
import cat.moon.azura.bean.AzProperty;
import cat.moon.azura.constant.ErrorCodeCommons;
import cat.moon.azura.constant.PropConstant;
import cat.moon.azura.exception.AzException;
import cat.moon.azura.service.ReportService;
import cat.moon.azura.util.PropUtil;
import cat.moon.azura.util.StringUtil;

public class ReportExportService extends ReportService {
	private static Logger logger = Logger.getLogger(ReportExportService.class);
	
	private static final int TXT_ID_LENGTH			= 50;
	private static final int TXT_IDNAME_LENGTH		= 50;
	private static final int TXT_NAME_LENGTH		= 100;
	
	private int txtIdLength;
	private int txtIdNameLength;
	private int txtNameLength;
	
	public ReportExportService(PropUtil prop, String reportFolder, String serviceName) throws AzException {
		super(prop, reportFolder, serviceName);
		if(generateTxt) {
			txtIdLength = prop.getInt(PropConstant.REPORT_GENERATE_TXT_ID_LENGTH, TXT_ID_LENGTH);
			txtIdNameLength = prop.getInt(PropConstant.REPORT_GENERATE_TXT_IDNAME_LENGTH, TXT_IDNAME_LENGTH);
			txtNameLength = prop.getInt(PropConstant.REPORT_GENERATE_TXT_NAME_LENGTH, TXT_NAME_LENGTH);
		}
	}
	
	protected void initializeFiles() throws AzException {
		super.initializeFiles();
		
		/* Generate CSV */
		try {
			//Header
			bw.write("id"+delimiter+"idName"+delimiter+"name\n");
			if(generateTxt) {
				bwTxt.write(StringUtil.addSpaces("ID", txtIdLength)+" ");
				bwTxt.write(StringUtil.addSpaces("ID Name", txtIdNameLength)+" ");
				bwTxt.write(StringUtil.addSpaces("Name", txtNameLength));
				bwTxt.write("\n");
				bwTxt.write(StringUtil.repeatChar("-", txtIdLength)+" ");
				bwTxt.write(StringUtil.repeatChar("-", txtIdNameLength)+" ");
				bwTxt.write(StringUtil.repeatChar("-", txtNameLength));
				bwTxt.write("\n");
			}
			
		} catch(Exception e) {
			throw new AzException(ErrorCodeCommons.AZCOM_09, e);
		}
	}
	

	public void generateDocumentClass(List<AzDocumentClass> list) throws AzException {
		if(list == null || list.isEmpty()) logger.info("No items found");
		else {
			try {
				initializeFiles();
				try {
					for (AzDocumentClass azList : list) {
						bw.write(azList.getId()+delimiter+azList.getIdName()+delimiter+azList.getName()+"\n");
						if(generateTxt) {
							bwTxt.write(StringUtil.addSpaces(azList.getId(), txtIdLength)+" ");
							bwTxt.write(StringUtil.addSpaces(azList.getIdName(), txtIdNameLength)+" ");
							bwTxt.write(StringUtil.addSpaces(azList.getName(), txtNameLength));
							bwTxt.write("\n");
						}
					}
				} catch (Exception e) {
					throw new AzException(ErrorCodeCommons.AZCOM_09, e);
				}
			} finally {
				close();
			}
		}
	}

	public void generateProperty(List<AzProperty> list) throws AzException {
		if(list == null || list.isEmpty()) logger.info("No items found");
		else {
			try {
				initializeFiles();
				try {
					for (AzProperty azList : list) {
						bw.write(azList.getId()+delimiter+azList.getIdName()+delimiter+azList.getName()+"\n");
						if(generateTxt) {
							bwTxt.write(StringUtil.addSpaces(azList.getId(), txtIdLength)+" ");
							bwTxt.write(StringUtil.addSpaces(azList.getIdName(), txtIdNameLength)+" ");
							bwTxt.write(StringUtil.addSpaces(azList.getName(), txtNameLength));
							bwTxt.write("\n");
						}
					}
				} catch (Exception e) {
					throw new AzException(ErrorCodeCommons.AZCOM_09, e);
				}
			} finally {
				close();
			}
		}
	}

	public void generateOptionList(List<AzOptionList> list) throws AzException {
		if(list == null || list.isEmpty()) logger.info("No items found");
		else {
			try {
				initializeFiles();
				try {
					for (AzOptionList azList : list) {
						bw.write(azList.getId()+delimiter+azList.getIdName()+delimiter+azList.getName()+"\n");
						if(generateTxt) {
							bwTxt.write(StringUtil.addSpaces(azList.getId(), txtIdLength)+" ");
							bwTxt.write(StringUtil.addSpaces(azList.getIdName(), txtIdNameLength)+" ");
							bwTxt.write(StringUtil.addSpaces(azList.getName(), txtNameLength));
							bwTxt.write("\n");
						}
					}
				} catch (Exception e) {
					throw new AzException(ErrorCodeCommons.AZCOM_09, e);
				}
			} finally {
				close();
			}
		}
	}
}
