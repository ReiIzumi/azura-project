package cat.moon.azura.azclass.constant;

import cat.moon.azura.bean.generic.ErrorMessage;

public class ErrorCodeClass {
	//File
	public static final ErrorMessage AZC_FI_01			= new ErrorMessage("AZC_FI_01", "Error preparing the file");
	public static final ErrorMessage AZC_FI_02			= new ErrorMessage("AZC_FI_02", "The file could not be renamed");
	public static final ErrorMessage AZC_FI_03			= new ErrorMessage("AZC_FI_03", "The JSON data does not contain the expected fomat");
	public static final ErrorMessage AZC_FI_04			= new ErrorMessage("AZC_FI_04", "The file could not be moved to Processed folder");
	public static final ErrorMessage AZC_FI_05			= new ErrorMessage("AZC_FI_05", "The file could not be moved to Error folder");
	
	//Document Class
	public static final ErrorMessage AZC_DC_01			= new ErrorMessage("AZC_DC_01", "Error writing the DocumentClass JSON export files");
	public static final ErrorMessage AZC_DC_02			= new ErrorMessage("AZC_DC_02", "Parent not found");
	
	//Property
	public static final ErrorMessage AZC_PR_01			= new ErrorMessage("AZC_PR_01", "Error writing the Property JSON export files");
	
	//Option List
	public static final ErrorMessage AZC_OL_01			= new ErrorMessage("AZC_OL_01", "Error writing the OptionList JSON export files");
	
	//Report
	public static final ErrorMessage AZC_RE_01			= new ErrorMessage("AZC_RE_01", "The action report could not be written");
	
	
}
