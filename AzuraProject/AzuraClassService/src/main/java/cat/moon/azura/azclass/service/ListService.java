package cat.moon.azura.azclass.service;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.util.Date;
import java.util.List;

import org.apache.log4j.Logger;

import cat.moon.azura.bean.generic.AzList;
import cat.moon.azura.constant.DelimiterConstant;
import cat.moon.azura.constant.ErrorCodeCommons;
import cat.moon.azura.constant.PropConstant;
import cat.moon.azura.exception.AzException;
import cat.moon.azura.util.FileUtil;
import cat.moon.azura.util.PropUtil;
import cat.moon.azura.util.StringUtil;

public class ListService {
	private static Logger logger = Logger.getLogger(ListService.class);
	
	private static final String CSV_EXTENSION		= "csv";
	private static final String TXT_EXTENSION		= "txt";
	private static final String FILENAME			= "List_";
	
	private static final int TXT_ID_LENGTH			= 50;
	private static final int TXT_IDNAME_LENGTH		= 50;
	private static final int TXT_NAME_LENGTH		= 100;
	
	private String delimiter;
	private String folderPath;
	private String datePattern;
	private boolean generateTxt;
	private Date date;
	private String serviceName;
	
	private int txtIdLength;
	private int txtIdNameLength;
	private int txtNameLength;
	
	public ListService(PropUtil prop, String listFolder, String serviceName) throws AzException {
		logger.info("> Initializing List Service");
		date = new Date();
		
		this.serviceName = serviceName;
		
		delimiter = prop.get(PropConstant.LIST_CSV_DELIMITER, DelimiterConstant.DELIMITER);
		folderPath = FileUtil.getFolderPath(prop.get(listFolder));
		logger.info(">> List folder: "+folderPath);
		datePattern = prop.get(PropConstant.LIST_FILE_DATEPATTERN, null);
		
		if("Y".equals(prop.get(PropConstant.LIST_GENERATE_TXT))) {
			generateTxt = true;
			txtIdLength = prop.getInt(PropConstant.LIST_GENERATE_TXT_ID_LENGTH, TXT_ID_LENGTH);
			txtIdNameLength = prop.getInt(PropConstant.LIST_GENERATE_TXT_IDNAME_LENGTH, TXT_IDNAME_LENGTH);
			txtNameLength = prop.getInt(PropConstant.LIST_GENERATE_TXT_NAME_LENGTH, TXT_NAME_LENGTH);
		}
		else generateTxt = false;
		
	}
	
	public void generate(List<AzList> list) throws AzException {
		if(list == null || list.isEmpty()) logger.info("No items found");
		else {
			if(logger.isDebugEnabled()) for (AzList azList : list) logger.debug(azList);
			
			/* List file */
			String filename = FileUtil.getFilename(FILENAME+serviceName, CSV_EXTENSION, datePattern, date);
			File file = FileUtil.getFile(folderPath, filename);
			FileUtil.deleteOldFile(file);
			
			String filenameTxt = null;
			File fileTxt = null;;
			if(generateTxt) {
				filenameTxt = FileUtil.getFilename(FILENAME+serviceName, TXT_EXTENSION, datePattern, date);
				fileTxt = FileUtil.getFile(folderPath, filenameTxt);
				FileUtil.deleteOldFile(fileTxt);
			}
			
			logger.info(">> List file: "+filename);
			if(generateTxt) logger.info(">> List file (TXT): "+filenameTxt);
			logger.info(">> Found: "+list.size());
			
			BufferedWriter bw = null;
			FileWriter fw = null;
			BufferedWriter bwTxt = null;
			FileWriter fwTxt = null;
			try {
				/* Prepare file */
				try {
					fw = new FileWriter(file);
					bw = new BufferedWriter(fw);
					if(generateTxt) {
						fwTxt = new FileWriter(fileTxt);
						bwTxt = new BufferedWriter(fwTxt);
					}
				} catch(Exception e) {
					throw new AzException(ErrorCodeCommons.AZCOM_08, e);
				}
				
				/* Generate CSV */
				try {
					//Header
					bw.write("id"+delimiter+"idName"+delimiter+"name\n");
					if(generateTxt) {
						bwTxt.write(StringUtil.addSpaces("ID", txtIdLength)+" ");
						bwTxt.write(StringUtil.addSpaces("ID Name", txtIdNameLength)+" ");
						bwTxt.write(StringUtil.addSpaces("Name", txtNameLength));
						bwTxt.write("\n");
						bwTxt.write(StringUtil.repeatChar("-", txtIdLength)+" ");
						bwTxt.write(StringUtil.repeatChar("-", txtIdNameLength)+" ");
						bwTxt.write(StringUtil.repeatChar("-", txtNameLength));
						bwTxt.write("\n");
					}
					
					for (AzList azList : list) {
						bw.write(azList.getId()+delimiter+azList.getIdName()+delimiter+azList.getName()+"\n");
						if(generateTxt) {
							bwTxt.write(StringUtil.addSpaces(azList.getId(), txtIdLength)+" ");
							bwTxt.write(StringUtil.addSpaces(azList.getIdName(), txtIdNameLength)+" ");
							bwTxt.write(StringUtil.addSpaces(azList.getName(), txtNameLength));
							bwTxt.write("\n");
						}
					}
				} catch(Exception e) {
					throw new AzException(ErrorCodeCommons.AZCOM_09, e);
				}
			} finally {
				if(bw != null) try {bw.close();} catch (Exception e) {}
				if(fw != null) try {fw.close();} catch (Exception e) {}
				if(bwTxt != null) try {bwTxt.close();} catch (Exception e) {}
				if(fwTxt != null) try {fwTxt.close();} catch (Exception e) {}
			}
		}
	}
}
