package cat.moon.azura.azclass.service;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;

import org.apache.log4j.Logger;

import cat.moon.azura.azclass.bean.ImportAction;
import cat.moon.azura.azclass.exception.ImportException;
import cat.moon.azura.azclass.exception.WarningException;
import cat.moon.azura.constant.ActionConstant;
import cat.moon.azura.constant.ErrorCodeCommons;
import cat.moon.azura.constant.PropConstant;
import cat.moon.azura.exception.AzException;
import cat.moon.azura.service.ReportService;
import cat.moon.azura.util.FileUtil;
import cat.moon.azura.util.PropUtil;
import cat.moon.azura.util.StringUtil;

public class ReportImportService extends ReportService {
	private static Logger logger = Logger.getLogger(ReportImportService.class);
	
	private static final String FILENAME_ACTION				= "Action_";
	private static final String FILENAME_ERROR				= "Error_";
	private static final String FILENAME_WARNING			= "Warning_";
	
	private static final int TXT_ID_LENGTH					= 50;
	private static final int TXT_IDNAME_LENGTH				= 50;
	private static final int TXT_NAME_LENGTH				= 100;

	private static final int TXT_TARGET_LENGTH				= 50;
	private static final int TXT_TIME_LENGTH				= 10;
	private static final int TXT_FILE_LENGTH				= 100;
	private static final int TXT_TYPE_LENGTH				= 13;
	private static final int TXT_ACTION_LENGTH				= 15;
	
	private static final int TXT_ERROR_CODE_LENGTH			= 15;
	private static final int TXT_ERROR_CODEMESSAGE_LENGTH	= 200;
	private static final int TXT_ERROR_MESSAGE_LENGTH		= 1000;
	
	private int txtIdLength;
	private int txtIdNameLength;
	private int txtNameLength;
	private int txtTargetLength;
	private int txtTimeLength;
	private int txtFileLength;
	private int txtTypeLength;
	private int txtActionLength;
	private int txtErrorCodeLength;
	private int txtErrorCodeMessageLength;
	private int txtErrorMessageLength;
	
	private BufferedWriter actionbw = null;
	private FileWriter actionfw = null;
	private BufferedWriter actionbwTxt = null;
	private FileWriter actionfwTxt = null;
	
	private BufferedWriter errorbw = null;
	private FileWriter errorfw = null;
	private BufferedWriter errorbwTxt = null;
	private FileWriter errorfwTxt = null;
	
	private File errorFile;
	private File errorFileTxt = null;
	
	private BufferedWriter warningbw = null;
	private FileWriter warningfw = null;
	private BufferedWriter warningbwTxt = null;
	private FileWriter warningfwTxt = null;
	
	private File warningFile;
	private File warningFileTxt = null;
	
	private int countError;
	private int countWarning;
	
	public ReportImportService(PropUtil prop, String reportFolder, String serviceName) throws AzException {
		super(prop, reportFolder, serviceName);
		if(generateTxt) {
			txtIdLength = prop.getInt(PropConstant.REPORT_GENERATE_TXT_ID_LENGTH, TXT_ID_LENGTH);
			txtIdNameLength = prop.getInt(PropConstant.REPORT_GENERATE_TXT_IDNAME_LENGTH, TXT_IDNAME_LENGTH);
			txtNameLength = prop.getInt(PropConstant.REPORT_GENERATE_TXT_NAME_LENGTH, TXT_NAME_LENGTH);
			txtTargetLength = prop.getInt(PropConstant.REPORT_GENERATE_TXT_TARGET_LENGTH, TXT_TARGET_LENGTH);
			txtTimeLength = prop.getInt(PropConstant.REPORT_GENERATE_TXT_TIME_LENGTH, TXT_TIME_LENGTH);
			txtFileLength = prop.getInt(PropConstant.REPORT_GENERATE_TXT_FILE_LENGTH, TXT_FILE_LENGTH);
			txtTypeLength = prop.getInt(PropConstant.REPORT_GENERATE_TXT_TYPE_LENGTH, TXT_TYPE_LENGTH);;
			txtActionLength = prop.getInt(PropConstant.REPORT_GENERATE_TXT_ACTION_LENGTH, TXT_ACTION_LENGTH);;
			txtErrorCodeLength = prop.getInt(PropConstant.REPORT_GENERATE_TXT_ERROR_CODE_LENGTH, TXT_ERROR_CODE_LENGTH);
			txtErrorCodeMessageLength = prop.getInt(PropConstant.REPORT_GENERATE_TXT_ERROR_CODEMESSAGE_LENGTH, TXT_ERROR_CODEMESSAGE_LENGTH);
			txtErrorMessageLength = prop.getInt(PropConstant.REPORT_GENERATE_TXT_ERROR_MESSAGE_LENGTH, TXT_ERROR_MESSAGE_LENGTH);
		}
		
		countError = 0;
		countWarning = 0;
	}
	
	public void initializeFiles() throws AzException {
		super.initializeFiles();
		
		//Generate Header
		try {
			bw.write("id"+delimiter+"idName"+delimiter+"name"+delimiter+"target"+delimiter+"time"+delimiter+"file\n");
			if(generateTxt) {
				bwTxt.write(StringUtil.addSpaces("ID", txtIdLength)+" ");
				bwTxt.write(StringUtil.addSpaces("ID Name", txtIdNameLength)+" ");
				bwTxt.write(StringUtil.addSpaces("Name", txtNameLength)+" ");
				bwTxt.write(StringUtil.addSpaces("Target", txtTargetLength)+" ");
				bwTxt.write(StringUtil.addSpaces("Time", txtTimeLength)+" ");
				bwTxt.write(StringUtil.addSpaces("File", txtFileLength)+" ");
				bwTxt.write("\n");
				bwTxt.write(StringUtil.repeatChar("-", txtIdLength)+" ");
				bwTxt.write(StringUtil.repeatChar("-", txtIdNameLength)+" ");
				bwTxt.write(StringUtil.repeatChar("-", txtNameLength)+" ");
				bwTxt.write(StringUtil.repeatChar("-", txtTargetLength)+" ");
				bwTxt.write(StringUtil.repeatChar("-", txtTimeLength)+" ");
				bwTxt.write(StringUtil.repeatChar("-", txtFileLength));
				bwTxt.write("\n");
			}
		} catch(Exception e) {
			throw new AzException(ErrorCodeCommons.AZCOM_09, e);
		}
		
		/* Action file */
		File actionFile = FileUtil.initializeFile(FILENAME_ACTION, serviceName, CSV_EXTENSION, datePattern, date, folderPath);
		File actionFileTxt = null;
		if(generateTxt) {
			actionFileTxt = FileUtil.initializeFile(FILENAME_ACTION, serviceName, TXT_EXTENSION, datePattern, date, folderPath);
		}
		
		logger.info(">> Report action file: "+actionFile.getName());
		if(generateTxt) logger.info(">> Report action file (TXT): "+actionFileTxt.getName());
		
		try {
			actionfw = new FileWriter(actionFile);
			actionbw = new BufferedWriter(actionfw);
			if(generateTxt) {
				actionfwTxt = new FileWriter(actionFileTxt);
				actionbwTxt = new BufferedWriter(actionfwTxt);
			}
		} catch(Exception e) {
			throw new AzException(ErrorCodeCommons.AZCOM_08, e);
		}
		
		//Generate Header
		try {
			actionbw.write("type"+delimiter+"action"+delimiter+"id"+delimiter+"idName"+delimiter+
					"name"+delimiter+"file\n");
			if(generateTxt) {
				actionbwTxt.write(StringUtil.addSpaces("Type", txtTypeLength)+" ");
				actionbwTxt.write(StringUtil.addSpaces("Action", txtActionLength)+" ");
				actionbwTxt.write(StringUtil.addSpaces("ID", txtIdLength)+" ");
				actionbwTxt.write(StringUtil.addSpaces("ID Name", txtIdNameLength)+" ");
				actionbwTxt.write(StringUtil.addSpaces("Name", txtNameLength)+" ");
				actionbwTxt.write(StringUtil.addSpaces("File", txtFileLength));
				actionbwTxt.write("\n");
				actionbwTxt.write(StringUtil.repeatChar("-", txtTypeLength)+" ");
				actionbwTxt.write(StringUtil.repeatChar("-", txtActionLength)+" ");
				actionbwTxt.write(StringUtil.repeatChar("-", txtIdLength)+" ");
				actionbwTxt.write(StringUtil.repeatChar("-", txtIdNameLength)+" ");
				actionbwTxt.write(StringUtil.repeatChar("-", txtNameLength)+" ");
				actionbwTxt.write(StringUtil.repeatChar("-", txtFileLength));
				actionbwTxt.write("\n");
			}
		} catch(Exception e) {
			throw new AzException(ErrorCodeCommons.AZCOM_09, e);
		}
		
		/* Error file */
		errorFile = FileUtil.initializeFile(FILENAME_ERROR, serviceName, CSV_EXTENSION, datePattern, date, folderPath);
		if(generateTxt) {
			errorFileTxt = FileUtil.initializeFile(FILENAME_ERROR, serviceName, TXT_EXTENSION, datePattern, date, folderPath);
		}
		
		if(logger.isDebugEnabled()) {
			logger.debug(">> Report error file: "+errorFile.getName());
			if(generateTxt) logger.debug(">> Report error file (TXT): "+errorFileTxt.getName());
		}
		
		try {
			errorfw = new FileWriter(errorFile);
			errorbw = new BufferedWriter(errorfw);
			if(generateTxt) {
				errorfwTxt = new FileWriter(errorFileTxt);
				errorbwTxt = new BufferedWriter(errorfwTxt);
			}
		} catch(Exception e) {
			throw new AzException(ErrorCodeCommons.AZCOM_08, e);
		}
		
		//Generate Header
		try {
			errorbw.write("id"+delimiter+"idName"+delimiter+"name"+delimiter+"file"+delimiter+
					"code"+delimiter+"code_message"+delimiter+"error_message\n");
			if(generateTxt) {
				errorbwTxt.write(StringUtil.addSpaces("ID", txtIdLength)+" ");
				errorbwTxt.write(StringUtil.addSpaces("ID Name", txtIdNameLength)+" ");
				errorbwTxt.write(StringUtil.addSpaces("Name", txtNameLength)+" ");
				errorbwTxt.write(StringUtil.addSpaces("File", txtFileLength)+" ");
				errorbwTxt.write(StringUtil.addSpaces("Code", txtErrorCodeLength)+" ");
				errorbwTxt.write(StringUtil.addSpaces("Code Message", txtErrorCodeMessageLength)+" ");
				errorbwTxt.write(StringUtil.addSpaces("Error Message", txtErrorMessageLength));
				errorbwTxt.write("\n");
				errorbwTxt.write(StringUtil.repeatChar("-", txtIdLength)+" ");
				errorbwTxt.write(StringUtil.repeatChar("-", txtIdNameLength)+" ");
				errorbwTxt.write(StringUtil.repeatChar("-", txtNameLength)+" ");
				errorbwTxt.write(StringUtil.repeatChar("-", txtFileLength)+" ");
				errorbwTxt.write(StringUtil.repeatChar("-", txtErrorCodeLength)+" ");
				errorbwTxt.write(StringUtil.repeatChar("-", txtErrorCodeMessageLength)+" ");
				errorbwTxt.write(StringUtil.repeatChar("-", txtErrorMessageLength));
				errorbwTxt.write("\n");
			}
		} catch(Exception e) {
			throw new AzException(ErrorCodeCommons.AZCOM_09, e);
		}
		
		/* Warning file */
		warningFile = FileUtil.initializeFile(FILENAME_WARNING, serviceName, CSV_EXTENSION, datePattern, date, folderPath);
		if(generateTxt) {
			warningFileTxt = FileUtil.initializeFile(FILENAME_WARNING, serviceName, TXT_EXTENSION, datePattern, date, folderPath);
		}
		
		if(logger.isDebugEnabled()) {
			logger.debug(">> Report warning file: "+warningFile.getName());
			if(generateTxt) logger.debug(">> Report warning file (TXT): "+warningFileTxt.getName());
		}
		
		try {
			warningfw = new FileWriter(warningFile);
			warningbw = new BufferedWriter(warningfw);
			if(generateTxt) {
				warningfwTxt = new FileWriter(warningFileTxt);
				warningbwTxt = new BufferedWriter(warningfwTxt);
			}
		} catch(Exception e) {
			throw new AzException(ErrorCodeCommons.AZCOM_08, e);
		}
		
		//Generate Header
		try {
			warningbw.write("file"+delimiter+"code"+delimiter+"code_message"+delimiter+"error_message\n");
			if(generateTxt) {
				warningbwTxt.write(StringUtil.addSpaces("File", txtFileLength)+" ");
				warningbwTxt.write(StringUtil.addSpaces("Code", txtErrorCodeLength)+" ");
				warningbwTxt.write(StringUtil.addSpaces("Code Message", txtErrorCodeMessageLength)+" ");
				warningbwTxt.write(StringUtil.addSpaces("Error Message", txtErrorMessageLength));
				warningbwTxt.write("\n");
				warningbwTxt.write(StringUtil.repeatChar("-", txtFileLength)+" ");
				warningbwTxt.write(StringUtil.repeatChar("-", txtErrorCodeLength)+" ");
				warningbwTxt.write(StringUtil.repeatChar("-", txtErrorCodeMessageLength)+" ");
				warningbwTxt.write(StringUtil.repeatChar("-", txtErrorMessageLength));
				warningbwTxt.write("\n");
			}
		} catch(Exception e) {
			throw new AzException(ErrorCodeCommons.AZCOM_09, e);
		}
	}
	
	public void add(String id, String idName, String name, String target, long time, String file) throws IOException {
		bw.write(id+delimiter+idName+delimiter+name+delimiter+target+delimiter+time+delimiter+file+"\n");
		
		if(generateTxt) {
			bwTxt.write(StringUtil.addSpaces(id, txtIdLength)+" ");
			bwTxt.write(StringUtil.addSpaces(idName, txtIdNameLength)+" ");
			bwTxt.write(StringUtil.addSpaces(name, txtNameLength)+" ");
			bwTxt.write(StringUtil.addSpaces(target, txtTargetLength)+" ");
			bwTxt.write(StringUtil.addSpaces(Long.toString(time), txtTimeLength)+" ");
			bwTxt.write(StringUtil.addSpaces(file, txtFileLength));
			bwTxt.write("\n");
		}
	}
	
	public void addAction(ImportAction iAct)  throws IOException {
		actionbw.write(iAct.getService()+delimiter+ActionConstant.TEXT.get(iAct.getAction())+delimiter+
				iAct.getImportResponse().getId()+delimiter+iAct.getImportResponse().getIdName()+delimiter+
				iAct.getImportResponse().getName()+delimiter+iAct.getFileName()+"\n");
		
		if(generateTxt) {
			actionbwTxt.write(StringUtil.addSpaces(iAct.getService(), txtTypeLength)+" ");
			actionbwTxt.write(StringUtil.addSpaces(ActionConstant.TEXT.get(iAct.getAction()), txtActionLength)+" ");
			actionbwTxt.write(StringUtil.addSpaces(iAct.getImportResponse().getId(), txtIdLength)+" ");
			actionbwTxt.write(StringUtil.addSpaces(iAct.getImportResponse().getIdName(), txtIdNameLength)+" ");
			actionbwTxt.write(StringUtil.addSpaces(iAct.getImportResponse().getName(), txtNameLength)+" ");
			actionbwTxt.write(StringUtil.addSpaces(iAct.getFileName(), txtFileLength));
			actionbwTxt.write("\n");
		}
	}
	
	public void addError(ImportException ie) throws IOException {
		errorbw.write(ie.getId()+delimiter+ie.getIdName()+delimiter+ie.getName()+delimiter+
				ie.getFilename()+delimiter+ie.getErrorMessage().getCode()+delimiter+
				ie.getErrorMessage().getMessage()+ie.getMessage()+"\n");
		
		if(generateTxt) {
			errorbwTxt.write(StringUtil.addSpaces(ie.getId(), txtIdLength)+" ");
			errorbwTxt.write(StringUtil.addSpaces(ie.getIdName(), txtIdNameLength)+" ");
			errorbwTxt.write(StringUtil.addSpaces(ie.getName(), txtNameLength)+" ");
			errorbwTxt.write(StringUtil.addSpaces(ie.getFilename(), txtFileLength)+" ");
			errorbwTxt.write(StringUtil.addSpaces(ie.getErrorMessage().getCode(), txtErrorCodeLength)+" ");
			errorbwTxt.write(StringUtil.addSpaces(ie.getErrorMessage().getMessage(), txtErrorCodeMessageLength)+" ");
			errorbwTxt.write(StringUtil.addSpaces(ie.getMessage(), txtErrorMessageLength));
			errorbwTxt.write("\n");
		}
		countError++;
	}

	public void addWarning(WarningException we) throws IOException {
		warningbw.write(we.getFilename()+delimiter+we.getErrorMessage().getCode()+delimiter+
				we.getErrorMessage().getMessage()+we.getMessage()+"\n");
		
		if(generateTxt) {
			warningbwTxt.write(StringUtil.addSpaces(we.getFilename(), txtFileLength)+" ");
			warningbwTxt.write(StringUtil.addSpaces(we.getErrorMessage().getCode(), txtErrorCodeLength)+" ");
			warningbwTxt.write(StringUtil.addSpaces(we.getErrorMessage().getMessage(), txtErrorCodeMessageLength)+" ");
			warningbwTxt.write(StringUtil.addSpaces(we.getMessage(), txtErrorMessageLength));
			warningbwTxt.write("\n");
		}
		countWarning++;
	}
	
	public void close() {
		super.close();
		
		if(actionbw != null) try {actionbw.close();} catch (Exception e) {}
		if(actionfw != null) try {actionfw.close();} catch (Exception e) {}
		if(actionbwTxt != null) try {actionbwTxt.close();} catch (Exception e) {}
		if(actionfwTxt != null) try {actionfwTxt.close();} catch (Exception e) {}
		
		if(errorbw != null) try {errorbw.close();} catch (Exception e) {}
		if(errorfw != null) try {errorfw.close();} catch (Exception e) {}
		if(errorbwTxt != null) try {errorbwTxt.close();} catch (Exception e) {}
		if(errorfwTxt != null) try {errorfwTxt.close();} catch (Exception e) {}
		
		if(warningbw != null) try {warningbw.close();} catch (Exception e) {}
		if(warningfw != null) try {warningfw.close();} catch (Exception e) {}
		if(warningbwTxt != null) try {warningbwTxt.close();} catch (Exception e) {}
		if(warningfwTxt != null) try {warningfwTxt.close();} catch (Exception e) {}
		
		if(errorFile != null) {
			if(countError == 0) {
				errorFile.delete();
				if(generateTxt) errorFileTxt.delete();
			}
		}
		if(warningFile != null) {
			if(countWarning == 0) {
				warningFile.delete();
				if(generateTxt) warningFileTxt.delete();
			}
		}
	}
}
