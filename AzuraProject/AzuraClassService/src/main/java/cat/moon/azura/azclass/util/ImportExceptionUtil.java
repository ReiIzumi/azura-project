package cat.moon.azura.azclass.util;

import java.io.File;
import java.io.IOException;

import org.apache.log4j.Logger;

import cat.moon.azura.azclass.exception.ImportException;
import cat.moon.azura.azclass.exception.WarningException;
import cat.moon.azura.azclass.service.ImportService;
import cat.moon.azura.azclass.service.ReportImportService;
import cat.moon.azura.bean.AzDocumentClass;
import cat.moon.azura.bean.AzOptionList;
import cat.moon.azura.bean.AzProperty;

public class ImportExceptionUtil {
	private static Logger logger = Logger.getLogger(ImportExceptionUtil.class);
	
	public static void generate(ImportException e, File file, AzDocumentClass bean, ImportService service, ReportImportService reportService) throws IOException {
		ErrorUtil.define(e, file, bean);
		
		//--If fail, move to error folder
		try {
			service.currentFileError();
		} catch(WarningException we) {
			ErrorUtil.define(we, file, bean);
			reportService.addWarning(we);
			logger.warn(we.getErrorMessage().getCode()+" - "+we.getErrorMessage().getMessage(), we);
		}
		
		//Add in report
		reportService.addError(e);
		
		logger.error(e.getErrorMessage().getCode()+" - "+e.getErrorMessage().getMessage(), e);
	}

	public static void generate(ImportException e, File file, AzProperty bean, ImportService service, ReportImportService reportService) throws IOException {
		ErrorUtil.define(e, file, bean);
		
		//--If fail, move to error folder
		try {
			service.currentFileError();
		} catch(WarningException we) {
			ErrorUtil.define(we, file, bean);
			reportService.addWarning(we);
			logger.warn(we.getErrorMessage().getCode()+" - "+we.getErrorMessage().getMessage(), we);
		}
		
		//Add in report
		reportService.addError(e);
		
		logger.error(e.getErrorMessage().getCode()+" - "+e.getErrorMessage().getMessage(), e);
	}
	
	public static void generate(ImportException e, File file, AzOptionList bean, ImportService service, ReportImportService reportService) throws IOException {
		ErrorUtil.define(e, file, bean);
		
		//--If fail, move to error folder
		try {
			service.currentFileError();
		} catch(WarningException we) {
			ErrorUtil.define(we, file, bean);
			reportService.addWarning(we);
			logger.warn(we.getErrorMessage().getCode()+" - "+we.getErrorMessage().getMessage(), we);
		}
		
		//Add in report
		reportService.addError(e);
		
		logger.error(e.getErrorMessage().getCode()+" - "+e.getErrorMessage().getMessage(), e);
	}

}
