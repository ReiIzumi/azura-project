package cat.moon.azura.azclass.controller;

import java.util.List;

import org.apache.log4j.Logger;

import cat.moon.azura.azclass.constant.Version;
import cat.moon.azura.azclass.service.ListService;
import cat.moon.azura.bean.generic.AzList;
import cat.moon.azura.constant.ErrorCodeCommons;
import cat.moon.azura.constant.PropConstant;
import cat.moon.azura.constant.ServiceConstant;
import cat.moon.azura.exception.AzException;
import cat.moon.azura.module.IClassModule;
import cat.moon.azura.service.ModuleService;
import cat.moon.azura.util.PropUtil;

public class ListDocumentClassController {
	private static Logger logger = Logger.getLogger(ListDocumentClassController.class);
	
	public static void start(PropUtil prop) throws AzException {
		IClassModule module = null;
		try {
			logger.info("============================== ==============================");
			logger.info("Starting Azura Class Service (v."+Version.VERSION+")");
			logger.info("- List all Document Class (v."+Version.LIST_DOCUMENT_CLASS+")");
			logger.info("============================== ==============================");
			
			/* Start service */
			ListService service = new ListService(prop, PropConstant.CLASS_LIST_FOLDER, ServiceConstant.DOCUMENT_CLASS);
			
			/* Get list from module */
			module = ModuleService.getClassModule(prop);
			
			//Get list from module
			List<AzList> list = module.listDocumentClass();
			
			/* Generate list */
			service.generate(list);
			
		} catch(AzException e) {
			throw e;
		} catch(Exception e) {
			throw new AzException(ErrorCodeCommons.DEFAULT_ERROR, e);
		} finally {
			if(module != null) module.close();
		}
	}
}
