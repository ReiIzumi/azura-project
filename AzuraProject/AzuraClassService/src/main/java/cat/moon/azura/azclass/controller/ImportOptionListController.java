package cat.moon.azura.azclass.controller;

import java.io.File;
import java.util.Date;
import java.util.Map;

import org.apache.log4j.Logger;

import cat.moon.azura.azclass.bean.ImportAction;
import cat.moon.azura.azclass.constant.ErrorCodeClass;
import cat.moon.azura.azclass.constant.Version;
import cat.moon.azura.azclass.exception.ImportException;
import cat.moon.azura.azclass.exception.WarningException;
import cat.moon.azura.azclass.service.ImportService;
import cat.moon.azura.azclass.service.ReportImportService;
import cat.moon.azura.azclass.util.ErrorUtil;
import cat.moon.azura.azclass.util.ImportExceptionUtil;
import cat.moon.azura.azclass.util.CurrentItemUtil;
import cat.moon.azura.bean.AzOptionList;
import cat.moon.azura.constant.ErrorCodeCommons;
import cat.moon.azura.constant.PropConstant;
import cat.moon.azura.constant.ServiceConstant;
import cat.moon.azura.exception.AzException;
import cat.moon.azura.module.IClassModule;
import cat.moon.azura.service.ModuleService;
import cat.moon.azura.util.PropUtil;

public class ImportOptionListController {
	private static Logger logger = Logger.getLogger(ImportOptionListController.class);
	
	public static void start(PropUtil prop) throws AzException {
		IClassModule module = null;
		ReportImportService reportService = null;
		try {
			logger.info("============================== ==============================");
			logger.info("Starting Azura Class Service (v."+Version.VERSION+")");
			logger.info("- Import Option List (v."+Version.IMPORT_OPTIONLIST+")");
			logger.info("============================== ==============================");
			
			/* Start import and report service */
			ImportService service = new ImportService(prop, 
					PropConstant.CLASS_IMPORT_SOURCE, PropConstant.CLASS_IMPORT_ERROR, 
					PropConstant.CLASS_IMPORT_PROCESSED, ServiceConstant.OPTION_LIST);
			reportService = new ReportImportService(prop, 
					PropConstant.REPORT_CLASS_IMPORT_FOLDER, ServiceConstant.OPTION_LIST);
			
			boolean updateMetadata = false;
			if("Y".equals(prop.get(PropConstant.IMPORT_CLASS_METADATA))) updateMetadata = true;
			
			/* Search files to import */
			int countFiles = service.searchFiles();
			if(countFiles == 0) {
				logger.info("> No files to import");
			} else {
				logger.info("> Files to import: "+countFiles);
				
				/* Start module */
				module = ModuleService.getClassModule(prop);
				
				/* Retrieve current items */
				String optionListIdField = CurrentItemUtil.checkId(module.getImportOptionListId());
				Map<String, Date> currentOptionLists = module.listCurrentOptionLists();
				
				/* Prepare report file */
				reportService.initializeFiles();
				
				/* Read files and import */
				AzOptionList bean;
				File file;
				long timer;
				String target;
				ImportAction importAction;
				while((file = service.nextFile()) != null) {
					bean = null;
					target = null;
					timer = new Date().getTime();
					try {
						//Load element
						bean = service.prepareOptionList(file);
						
						//Prepare to import
						module.importPrepare();
						
						//-- OptionList
						importAction = service.importOptionList(bean, optionListIdField, 
								currentOptionLists, updateMetadata, module);
						importAction.setFileName(file.getName());
						target = importAction.getImportResponse().getId();
						
						//Import changes
						module.importSave();
						
						//Add to report actions
						try {
							reportService.addAction(importAction);
						} catch(Exception e) {
							WarningException we = new WarningException(ErrorCodeClass.AZC_RE_01, e);
							ErrorUtil.define(we, file, bean);
							reportService.addWarning(we);
							logger.warn(we.getErrorMessage().getCode()+" - "+we.getErrorMessage().getMessage(), we);
							try {logger.warn(importAction);} catch(Exception e1) {}
						}
						
						//Move to processed
						try {
							service.currentFileProcessed();
						} catch(WarningException we) {
							ErrorUtil.define(we, file, bean);
							reportService.addWarning(we);
							logger.warn(we.getErrorMessage().getCode()+" - "+we.getErrorMessage().getMessage(), we);
						}
						
						//Add in report
						reportService.add(bean.getId(), bean.getIdName(), bean.getName(), target, new Date().getTime()-timer, file.getName());
					} catch(ImportException e) {
						ImportExceptionUtil.generate(e, file, bean, service, reportService);
					} catch(Exception e) {
						ImportException ie = new ImportException(ErrorCodeCommons.DEFAULT_ERROR, e);
						ImportExceptionUtil.generate(ie, file, bean, service, reportService);
					}
				}
			}
			
			logger.info("Process end");
		} catch(AzException e) {
			throw e;
		} catch(Exception e) {
			throw new AzException(ErrorCodeCommons.DEFAULT_ERROR, e);
		} finally {
			if(module != null) module.close();
			if(reportService != null) reportService.close();
		}
	}
}
