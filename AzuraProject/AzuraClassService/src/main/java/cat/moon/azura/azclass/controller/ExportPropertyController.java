package cat.moon.azura.azclass.controller;

import java.util.List;

import org.apache.log4j.Logger;

import cat.moon.azura.azclass.constant.Version;
import cat.moon.azura.azclass.service.ExportService;
import cat.moon.azura.azclass.service.ReportExportService;
import cat.moon.azura.bean.AzProperty;
import cat.moon.azura.constant.ErrorCodeCommons;
import cat.moon.azura.constant.PropConstant;
import cat.moon.azura.constant.ServiceConstant;
import cat.moon.azura.exception.AzException;
import cat.moon.azura.module.IClassModule;
import cat.moon.azura.service.ModuleService;
import cat.moon.azura.util.PropUtil;

public class ExportPropertyController {
	private static Logger logger = Logger.getLogger(ExportPropertyController.class);
	
	public static void start(PropUtil prop, List<String> ids) throws AzException {
		IClassModule module = null;
		try {
			logger.info("============================== ==============================");
			logger.info("Starting Azura Class Service (v."+Version.VERSION+")");
			logger.info("- Export Properties (v."+Version.EXPORT_PROPERTIES+")");
			logger.info("============================== ==============================");
			
			/* Start export and report service */
			ExportService service = new ExportService(prop, 
					PropConstant.CLASS_EXPORT_FOLDER, ServiceConstant.PROPERTY);
			ReportExportService reportService = new ReportExportService(prop, 
					PropConstant.REPORT_CLASS_EXPORT_FOLDER, ServiceConstant.PROPERTY);
			
			/* Start module */
			module = ModuleService.getClassModule(prop);
			
			/* Export from module */
			List<AzProperty> list = module.exportProperty(ids);
			
			/* Generate files and report */
			service.generateProperty(list);
			reportService.generateProperty(list);
			
		} catch(AzException e) {
			throw e;
		} catch(Exception e) {
			throw new AzException(ErrorCodeCommons.DEFAULT_ERROR, e);
		} finally {
			if(module != null) module.close();
		}
	}
}
