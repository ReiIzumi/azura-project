package cat.moon.azura.azclass.service;

import java.io.File;
import java.util.List;

import org.apache.commons.io.FileUtils;
import org.apache.log4j.Logger;

import com.fasterxml.jackson.databind.ObjectMapper;

import cat.moon.azura.azclass.constant.ErrorCodeClass;
import cat.moon.azura.bean.AzDocumentClass;
import cat.moon.azura.bean.AzOptionList;
import cat.moon.azura.bean.AzProperty;
import cat.moon.azura.exception.AzException;
import cat.moon.azura.util.FileUtil;
import cat.moon.azura.util.PropUtil;

public class ExportService {
	private static Logger logger = Logger.getLogger(ExportService.class);
	
	public static final String FILENAME				= "Export_";
	private static final String EXTENSION			= "json";
	
	private String folderPath;
	private String serviceName;
	
	public ExportService(PropUtil prop, String exportFolder, String serviceName) throws AzException {
		logger.info("> Initializing Export Service");
		this.serviceName = serviceName;
		
		folderPath = FileUtil.getFolderPath(prop.get(exportFolder));
		logger.info(">> Export folder: "+folderPath);
	}
	
	public void generateDocumentClass(List<AzDocumentClass> list) throws AzException {
		if(list == null || list.isEmpty()) logger.info("No items found");
		else {
			if(logger.isTraceEnabled()) for (AzDocumentClass azList : list) logger.trace(azList);
			ObjectMapper mapper = new ObjectMapper();
			
			String filename;
			for (AzDocumentClass azProperty : list) {
				try {
					logger.info("Exporting: "+azProperty.getId()+" / "+azProperty.getIdName()+" / "+azProperty.getName());
					filename = FileUtil.getFilename(FILENAME+serviceName+"_"+azProperty.getId(), EXTENSION, null);
					FileUtils.writeStringToFile(
							new File(folderPath+filename), 
							mapper.writeValueAsString(azProperty), 
							"UTF-8", 
							false);
				} catch(Exception e) {
					throw new AzException(ErrorCodeClass.AZC_DC_01.extraMessage(azProperty.getId()), e);
				}
			}
		}
	}
	
	public void generateProperty(List<AzProperty> list) throws AzException {
		if(list == null || list.isEmpty()) logger.info("No items found");
		else {
			if(logger.isTraceEnabled()) for (AzProperty azList : list) logger.trace(azList);
			ObjectMapper mapper = new ObjectMapper();
			
			String filename;
			for (AzProperty azProperty : list) {
				try {
					logger.info("Exporting: "+azProperty.getId()+" / "+azProperty.getIdName()+" / "+azProperty.getName());
					filename = FileUtil.getFilename(FILENAME+serviceName+"_"+azProperty.getId(), EXTENSION, null);
					FileUtils.writeStringToFile(
							new File(folderPath+filename), 
							mapper.writeValueAsString(azProperty), 
							"UTF-8", 
							false);
				} catch(Exception e) {
					throw new AzException(ErrorCodeClass.AZC_PR_01.extraMessage(azProperty.getId()), e);
				}
			}
		}
	}

	public void generateOptionList(List<AzOptionList> list) throws AzException {
		if(list == null || list.isEmpty()) logger.info("No items found");
		else {
			if(logger.isTraceEnabled()) for (AzOptionList azList : list) logger.trace(azList);
			ObjectMapper mapper = new ObjectMapper();
			
			String filename;
			for (AzOptionList azProperty : list) {
				try {
					logger.info("Exporting: "+azProperty.getId()+" / "+azProperty.getIdName()+" / "+azProperty.getName());
					filename = FileUtil.getFilename(FILENAME+serviceName+"_"+azProperty.getId(), EXTENSION, null);
					FileUtils.writeStringToFile(
							new File(folderPath+filename), 
							mapper.writeValueAsString(azProperty), 
							"UTF-8", 
							false);
				} catch(Exception e) {
					throw new AzException(ErrorCodeClass.AZC_OL_01.extraMessage(azProperty.getId()), e);
				}
			}
		}
	}
}
