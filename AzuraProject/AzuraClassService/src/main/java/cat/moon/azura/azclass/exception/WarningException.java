package cat.moon.azura.azclass.exception;

import cat.moon.azura.bean.generic.ErrorMessage;

public class WarningException extends ImportException {
	private static final long serialVersionUID = 2526006464003476994L;
	
	public WarningException(ErrorMessage errorMessage) {
		super(errorMessage);
	}
	
	public WarningException(ErrorMessage errorMessage, String message, Throwable cause) {
		super(errorMessage, message, cause);
	}

	public WarningException(ErrorMessage errorMessage, String message) {
		super(errorMessage, message);
	}

	public WarningException(ErrorMessage errorMessage, Throwable cause) {
		super(errorMessage, cause);
	}
}
