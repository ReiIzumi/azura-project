package cat.moon.azura.azclass.controller;

import java.io.File;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;

import org.apache.log4j.Logger;

import cat.moon.azura.azclass.bean.ImportAction;
import cat.moon.azura.azclass.constant.ErrorCodeClass;
import cat.moon.azura.azclass.constant.Version;
import cat.moon.azura.azclass.exception.ImportException;
import cat.moon.azura.azclass.exception.WarningException;
import cat.moon.azura.azclass.service.ImportService;
import cat.moon.azura.azclass.service.ReportImportService;
import cat.moon.azura.azclass.util.ErrorUtil;
import cat.moon.azura.azclass.util.ImportExceptionUtil;
import cat.moon.azura.azclass.util.CurrentItemUtil;
import cat.moon.azura.bean.AzProperty;
import cat.moon.azura.constant.ErrorCodeCommons;
import cat.moon.azura.constant.PropConstant;
import cat.moon.azura.constant.ServiceConstant;
import cat.moon.azura.exception.AzException;
import cat.moon.azura.module.IClassModule;
import cat.moon.azura.service.ModuleService;
import cat.moon.azura.util.PropUtil;

public class ImportPropertyController {
	private static Logger logger = Logger.getLogger(ImportPropertyController.class);
	
	public static void start(PropUtil prop) throws AzException {
		IClassModule module = null;
		ReportImportService reportService = null;
		try {
			logger.info("============================== ==============================");
			logger.info("Starting Azura Class Service (v."+Version.VERSION+")");
			logger.info("- Import Property (v."+Version.IMPORT_PROPERTIES+")");
			logger.info("============================== ==============================");
			
			/* Start import and report service */
			ImportService service = new ImportService(prop, 
					PropConstant.CLASS_IMPORT_SOURCE, PropConstant.CLASS_IMPORT_ERROR, 
					PropConstant.CLASS_IMPORT_PROCESSED, ServiceConstant.PROPERTY);
			reportService = new ReportImportService(prop, 
					PropConstant.REPORT_CLASS_IMPORT_FOLDER, ServiceConstant.PROPERTY);
			
			boolean updateMetadata = false;
			if("Y".equals(prop.get(PropConstant.IMPORT_CLASS_METADATA))) updateMetadata = true;
			
			/* Search files to import */
			int countFiles = service.searchFiles();
			if(countFiles == 0) {
				logger.info("> No files to import");
			} else {
				logger.info("> Files to import: "+countFiles);
				
				/* Start module */
				module = ModuleService.getClassModule(prop);
				
				/* Retrieve current items */
				String propertyIdField = CurrentItemUtil.checkId(module.getImportPropertyId());
				String optionListIdField = CurrentItemUtil.checkId(module.getImportOptionListId());
				Map<String, Date> currentProperties = module.listCurrentProperties();
				Map<String, Date> currentOptionLists = module.listCurrentOptionLists();
				
				/* Prepare report file */
				reportService.initializeFiles();
				
				/* Read files and import */
				AzProperty bean;
				File file;
				long timer;
				List<ImportAction> importActions;
				ImportAction importAction;
				String target;
				
				while((file = service.nextFile()) != null) {
					bean = null;
					target = null;
					timer = new Date().getTime();
					importActions = new ArrayList<ImportAction>();
					try {
						//Load element
						bean = service.prepareProperty(file);
						
						//Prepare to import
						module.importPrepare();
						
						//-- OptionList
						if(bean.getOptionList() != null) {
							importAction = service.importOptionList(bean.getOptionList(), optionListIdField, 
									currentOptionLists, updateMetadata, module);
							importAction.setFileName(file.getName());
							importActions.add(importAction);
						}
						
						//-- Property
						importAction = service.importProperty(bean, propertyIdField, 
								currentProperties, updateMetadata, module);
						importAction.setFileName(file.getName());
						target = importAction.getImportResponse().getId();
						importActions.add(importAction);
						
						//Import changes
						module.importSave();
						
						//Add to report actions
						try {
							for (ImportAction iAct : importActions) {
								reportService.addAction(iAct);
							}
						} catch(Exception e) {
							WarningException we = new WarningException(ErrorCodeClass.AZC_RE_01, e);
							ErrorUtil.define(we, file, bean);
							reportService.addWarning(we);
							logger.warn(we.getErrorMessage().getCode()+" - "+we.getErrorMessage().getMessage(), we);
							for (ImportAction iAct : importActions) {
								try {logger.warn(iAct);} catch(Exception e1) {}
							}
						}
						
						//Move to processed
						try {
							service.currentFileProcessed();
						} catch(WarningException we) {
							ErrorUtil.define(we, file, bean);
							reportService.addWarning(we);
							logger.warn(we.getErrorMessage().getCode()+" - "+we.getErrorMessage().getMessage(), we);
						}
						
						//Add in report
						reportService.add(bean.getId(), bean.getIdName(), bean.getName(), target, new Date().getTime()-timer, file.getName());
					} catch(ImportException e) {
						ImportExceptionUtil.generate(e, file, bean, service, reportService);
					} catch(Exception e) {
						ImportException ie = new ImportException(ErrorCodeCommons.DEFAULT_ERROR, e);
						ImportExceptionUtil.generate(ie, file, bean, service, reportService);
					}
				}
			}
			logger.info("Process end");
		} catch(AzException e) {
			throw e;
		} catch(Exception e) {
			throw new AzException(ErrorCodeCommons.DEFAULT_ERROR, e);
		} finally {
			if(module != null) module.close();
			if(reportService != null) reportService.close();
		}
	}
}
