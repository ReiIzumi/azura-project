package cat.moon.azura.azclass.bean.config;

import java.io.Serializable;
import java.util.List;
import java.util.Vector;

public class ExportConfig implements Serializable {
	private static final long serialVersionUID = -5025919632562076427L;
	
	private List<String> symbolicNames;
	private Vector<String> symbolicNamesExclude;
	private boolean exportChild;
	
	public List<String> getSymbolicNames() {
		return symbolicNames;
	}
	public void setSymbolicNames(List<String> symbolicNames) {
		this.symbolicNames = symbolicNames;
	}
	public Vector<String> getSymbolicNamesExclude() {
		return symbolicNamesExclude;
	}
	public void setSymbolicNamesExclude(Vector<String> symbolicNamesExclude) {
		this.symbolicNamesExclude = symbolicNamesExclude;
	}
	public boolean isExportChild() {
		return exportChild;
	}
	public void setExportChild(boolean exportChild) {
		this.exportChild = exportChild;
	}
	
	@Override
	public String toString() {
		return "ExportConfig [symbolicNames=" + symbolicNames + ", symbolicNamesExclude=" + symbolicNamesExclude
				+ ", exportChild=" + exportChild + "]";
	}
}
