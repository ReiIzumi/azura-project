package cat.moon.azura.azclass.controller;

import org.apache.log4j.Logger;

import cat.moon.azura.bean.generic.ErrorMessage;
import cat.moon.azura.constant.ErrorCodeCommons;
import cat.moon.azura.exception.AzException;
import cat.moon.azura.init.ErrorExitcodeInit;
import cat.moon.azura.init.Log4jInit;
import cat.moon.azura.init.PropertiesInit;
import cat.moon.azura.util.PropUtil;

public class ImportDocumentClass {
	private static Logger logger = Logger.getLogger(ImportDocumentClass.class);
	
	public static void main(String[] args) {
		boolean logStarted = false;
		
		PropUtil errExitcodeProp = null;
		try {
			if(args.length != 1) {
				System.out.println("Arguments invalid");
				System.exit(ErrorCodeCommons.START_EXITCODE);
			}
			
			/* Load properties file */
			PropUtil propUtil = PropertiesInit.init(args[0]);
			
			/* Load log4j properties */
			Log4jInit.init(propUtil);
			logStarted = true;
			
			/* Load Error message */
			errExitcodeProp = ErrorExitcodeInit.init(propUtil);
			
			/* Start service */
			ImportDocumentClassController.start(propUtil);
		} catch(AzException e) {
			if(logStarted) {
				logger.error(e.getErrorMessage().getCode()+" - "+e.getErrorMessage().getMessage(), e);
			} else {
				System.out.println(e.getErrorMessage().getCode()+" - "+e.getErrorMessage().getMessage());
				e.printStackTrace();
			}
			if(errExitcodeProp != null && errExitcodeProp.contains(e.getErrorMessage().getCode())) {
				System.exit(errExitcodeProp.getInt(e.getErrorMessage().getCode(), e.getErrorMessage().getExitCode()));
			} else System.exit(e.getErrorMessage().getExitCode());
		} catch(Exception e) {
			ErrorMessage message = ErrorCodeCommons.DEFAULT_ERROR;
			if(logStarted) {
				logger.error(message.getCode()+" - "+message.getMessage(), e);
			} else {
				System.out.println(message.getCode()+" - "+message.getMessage());
				e.printStackTrace();
			}
			if(errExitcodeProp != null && errExitcodeProp.contains(message.getCode())) {
				System.exit(errExitcodeProp.getInt(message.getCode(), message.getExitCode()));
			} else System.exit(message.getExitCode());
		}
		System.exit(0);
	}
}
